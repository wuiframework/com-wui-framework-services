# * ********************************************************************************************************* *
# *
# * Copyright (c) 2017-2018 NXP
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

cmake_minimum_required(VERSION 3.6)
project(FFIProxyLib)

set(CMAKE_CXX_STANDARD 14)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/build)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/build)

if (WIN32 OR WIN64)
    message(status "Selected WIN platform")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -static")
    set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -static")
    set(SOURCE_FILES dllmain.cpp FFIProxyLib.hpp FFIProxyLib.cpp)
    add_definitions(-DWIN_PLATFORM)
else ()
    message(status "Selected Linux platform")
    set(CMAKE_POSITION_INDEPENDENT_CODE ON)
    set(SOURCE_FILES FFIProxyLib.hpp FFIProxyLib.cpp)
    add_definitions(-DLINUX_PLATFORM)
endif()

add_library(FFIProxyLib SHARED ${SOURCE_FILES})
