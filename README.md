# com-wui-framework-services v2019.1.0

> WUI Framework library focused on services and business logic for the applications built on the WUI Framework.

## Requirements

This library does not have any special requirements, but it depends on the 
[WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder). See the WUI Builder requirements before you build this project.

## Project build

The project build is fully automated. For more information about the project build, see the 
[WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder) documentation.

## Documentation

This project provides automatically generated documentation in [TypeDoc](http://typedoc.org/) from the TypeScript source by running the 
`wui docs` command from the {projectRoot} folder.

> NOTE: The documentation is accessible also from the {projectRoot}/build/target/docs/index.html file after a successful creation.

## History

### v2019.1.0
Added support for buffer IO args. FFI refactoring, FS and Terminal connectors updates. Removed invoke of client close from WS communication. 
Updated static CSS for BasePageController.
### v2019.0.2
Usage of API for custom connectors Error handlers. Project settings update and usage of new app loader.
### v2019.0.1
Added ability to control connectors error propagation and handling of reconnects count. 
Enable to handle individual errors for each connector invoke protocol. Usage of hub property instead of hardcoded url.
Added ability to use WUI Localhost as WUI Connector interface. Added unload resource API.
### v2019.0.0
Fixed and updated installation protocols. Added Certs connector. Added support for read/write files in stream format. 
Added ability to modify DAO source path location on the fly.
### v2018.3.0
Support for McbPcm API moved to MCAT project. Update of connectors used in WUI Builder. Tests update. Added integration of WUI Hub. 
Added forwarding client for communication over WUI Hub. Removed 7-Zip support. Updated of FFI proxy API. 
Added support for upload of packages with specific type.
### v2018.2.0
Updates required by better code sharing at back-end projects. Updated FFIProxy interfaces. 
Fixed issues connected with messages forwarding by LiveContentProtocol. Performance increase by usage of better DOM cache.
### v2018.1.3
Added default settings for viewport. Temporary fix for validation of origin on real domain.
### v2018.1.2
Fixed Pack promise API. Added more robust install condition for PortableGit. Added simple unit test for InstallationProtocol.
### v2018.1.1
API and code clean up. Added helper method for handling of success controller events.
### v2018.1.0
Update of WUI Core for ability to support multithreading model. API clean up. 
Usage of BaseConnector for simplification of Connectors creation and runtime switch handling. Added support for FFIProxy in WUI Builder.
### v2018.0.3
Integration of VBox manager API. Fixed merge of arrays in JSONP configuration. Update of MSYS2 and CMake installation protocol.
### v2018.0.2
Added installation protocols for portable GIT, CMake and MSYS2. Bug fixes and enhancements in base installation recipes.
### v2018.0.1
Fixed issues with filter effect on ChromiumRE. Refactoring of DLLProxy into the FFIProxy. 
Added ability to specify environment variables for Terminal process. Fixed linting issues.
### v2018.0.0
Update of Webservice API required by SSL communication. Change version format.
### v2.2.2
Added full support for resizable desktop RE. Window resize and drag is handled over CEF query now.
### v2.2.1
Added support for manage of native directory browser dialog in ChromiumRE.
### v2.2.0
Bug fixes for consumption of terminal std and installation protocol. 
Added simple networking connector and test for remote FS synchronization. Update of SCR and change of history ordering.
### v2.1.1
Convert of configs from XML to JSONP. Fixed tests for compatibility with linux.
### v2.1.0
Upgrade of connectors based on updated back-ends for desktop and plugins. Update of Commons and GUI libraries.
### v2.0.3
Added support for multiple clients connection. Added fixed version to dependencies.
### v2.0.2
Update of FileSystemHandlerConnector API and tests. Clean up of selfextractor configuration.
### v2.0.1
Update of Typescript syntax. Update of REST and desktop connectors. 
### v2.0.0
Namespaces refactoring.
### v1.1.0
Changed the licence from proprietary to BSD-3-Clause.
### v1.0.0
Initial release.

## License

This software is owned or controlled by NXP Semiconductors. 
The use of this software is governed by the BSD-3-Clause Licence distributed with this material.
  
See the `LICENSE.txt` file for more details.

---

Author Jakub Cieslar, 
Copyright (c) 2014-2016 [Freescale Semiconductor, Inc.](http://freescale.com/), 
Copyright (c) 2017-2019 [NXP](http://nxp.com/)
