/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Services.HttpProcessor {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import TerminalConnector = Com.Wui.Framework.Services.Connectors.TerminalConnector;

    export class HttpManager extends Com.Wui.Framework.Gui.HttpProcessor.HttpManager {
        private connector : TerminalConnector;

        public ReloadTo($link? : string, $data? : any, $async? : boolean) : void {
            if (this.getRequest().IsWuiJre()
                && !ObjectValidator.IsEmptyOrNull($link) && ObjectValidator.IsBoolean($data) && $data) {
                if (!ObjectValidator.IsSet(this.connector)) {
                    this.connector = new TerminalConnector();
                }
                this.connector.Open($link);
            } else {
                super.ReloadTo($link, $data, $async);
            }
        }
    }
}
