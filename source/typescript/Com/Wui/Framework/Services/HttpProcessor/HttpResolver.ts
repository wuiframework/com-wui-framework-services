/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Services.HttpProcessor {
    "use strict";

    export class HttpResolver extends Com.Wui.Framework.Gui.HttpProcessor.HttpResolver {

        protected getStartupResolvers() : any {
            const resolvers : any = super.getStartupResolvers();
            resolvers["/ServerError/Http/DefaultPage"] = Com.Wui.Framework.Services.Index;
            resolvers["/"] = Com.Wui.Framework.Services.Index;
            resolvers["/index"] = Com.Wui.Framework.Services.Index;
            resolvers["/web/"] = Com.Wui.Framework.Services.Index;
            resolvers["/ServerError/Http/NotAuthorized"] = Com.Wui.Framework.Services.ErrorPages.AuthHttpResolver;
            resolvers["/ServerError/Http/ConnectionLost"] = Com.Wui.Framework.Services.ErrorPages.ConnectionLostPage;
            /* dev:start */
            resolvers["/about/Package"] = Com.Wui.Framework.Services.HttpProcessor.Resolvers.AboutPackage;
            resolvers["/PageControllerTest"] = Com.Wui.Framework.Services.RuntimeTests.PageControllerTest;
            /* dev:end */
            return resolvers;
        }

        protected getHttpManagerClass() : any {
            return HttpManager;
        }
    }
}
