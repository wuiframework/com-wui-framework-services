/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Services.HttpProcessor.Resolvers {
    "use strict";
    import TimeoutManager = Com.Wui.Framework.Commons.Events.TimeoutManager;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;
    import ErrorPageException = Com.Wui.Framework.Commons.Exceptions.Type.ErrorPageException;
    import HttpRequestConstants = Com.Wui.Framework.Gui.Enums.HttpRequestConstants;
    import Exception = Com.Wui.Framework.Commons.Exceptions.Type.Exception;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;

    /**
     * ExceptionPageController class provides handling of requests for Exception page controller.
     */
    export class ExceptionPageController extends BasePageController {
        private trace : string;

        public Process() : void {
            try {
                ExceptionsManager.ThrowExit();
            } catch (ex) {
                // stop all background execution, but continue in execution of current resolver
            }
            try {
                super.Process();
            } catch (ex) {
                try {
                    ExceptionsManager.Throw(this.getClassName(), ex);
                } catch (ex) {
                    // register error page self-error and continue with processing of general exception manager
                }
                try {
                    ExceptionsManager.Throw(this.getClassName(), new ErrorPageException(this.getClassName() + " self error."));
                } catch (ex) {
                    ExceptionsManager.HandleException(ex);
                }
            }
        }

        protected argsHandler($GET : ArrayList<string>, $POST : ArrayList<any>) : void {
            this.trace = "";
            if ($POST.KeyExists(HttpRequestConstants.EXCEPTIONS_LIST)) {
                const exceptions : ArrayList<Exception> = $POST.getItem(HttpRequestConstants.EXCEPTIONS_LIST);
                try {
                    this.trace = exceptions.getFirst().ToString("", false);
                } catch (ex) {
                    // ignore processing of stack trace, if convert to string is not possible
                }
                const printer : TimeoutManager = new TimeoutManager();
                let errorsOutput : string = "";
                let index : number;
                for (index = 0; index < exceptions.Length(); index++) {
                    printer.Add(($index : number) : void => {
                        const exception : Exception = exceptions.getItem($index);
                        try {
                            errorsOutput += "thrown by: " + exception.Owner() + StringUtils.NewLine(false);
                            errorsOutput += exception.ToString("", false) + StringUtils.NewLine(false);
                        } catch (ex) {
                            errorsOutput += "thrown by: exceptions printer " + StringUtils.NewLine(false);
                            errorsOutput += ex.message + StringUtils.NewLine(false);
                        }
                    });
                }
                printer.Add(() : void => {
                    LogIt.Error(errorsOutput);
                    ExceptionsManager.Clear();
                });
                printer.Execute();
            } else if ($POST.KeyExists(HttpRequestConstants.HTTP404_FILE_PATH)) {
                LogIt.Error("Unable to find file: " + $POST.getItem(HttpRequestConstants.HTTP404_FILE_PATH));
            } else {
                LogIt.Error("Error page reached for request: " + this.getRequest().getUrl());
                LogIt.Debug(StringUtils.StripTags(StringUtils.Replace(this.getRequest().ToString(),
                    StringUtils.NewLine(), StringUtils.NewLine(false))));
            }
        }

        protected getTrace() : string {
            return this.trace;
        }
    }
}
