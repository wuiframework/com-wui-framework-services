/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Services.ErrorPages {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IWebServiceClient = Com.Wui.Framework.Services.Interfaces.IWebServiceClient;
    import BaseErrorPage = Com.Wui.Framework.Gui.ErrorPages.BaseErrorPage;

    export class ConnectionLostPage extends BaseErrorPage {

        protected getPageTitle() : string {
            return "WUI - Connection Lost";
        }

        protected getMessageHeader() : string {
            return "Service is unreachable";
        }

        protected getMessageBody() : string {
            return "Service request done by: " + this.RequestArgs().POST().getItem("Owner") + ", " +
                "to: " + this.RequestArgs().POST().getItem("Client").getServerUrl();
        }

        protected getOwner() : string {
            return this.RequestArgs().POST().getItem("Owner");
        }

        protected restart() : void {
            const client : IWebServiceClient = this.RequestArgs().POST().getItem("Client");
            client.StopCommunication();
            client.StartCommunication();
        }
    }
}
