/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Services.ErrorPages {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IWebServiceProtocol = Com.Wui.Framework.Services.Interfaces.IWebServiceProtocol;
    import IWebServiceClient = Com.Wui.Framework.Services.Interfaces.IWebServiceClient;
    import ObjectDecoder = Com.Wui.Framework.Commons.Utils.ObjectDecoder;
    import BaseErrorPage = Com.Wui.Framework.Gui.ErrorPages.BaseErrorPage;
    import IPersistenceHandler = Com.Wui.Framework.Commons.Interfaces.IPersistenceHandler;
    import PersistenceFactory = Com.Wui.Framework.Commons.PersistenceApi.PersistenceFactory;
    import PersistenceType = Com.Wui.Framework.Gui.Enums.PersistenceType;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import HttpStatusType = Com.Wui.Framework.Commons.Enums.HttpStatusType;

    export class AuthHttpResolver extends BaseErrorPage {
        private tokenExpiration : string;

        public static LogOut() : void {
            PersistenceFactory.getPersistence(PersistenceType.CLIENT_IP, AuthHttpResolver.ClassName()).Destroy("token");
        }

        public static getToken() : string {
            const token : string = PersistenceFactory.getPersistence(PersistenceType.CLIENT_IP, AuthHttpResolver.ClassName())
                .Variable("token");
            return ObjectValidator.IsEmptyOrNull(token) ? "" : token;
        }

        protected getFaviconSource() : string {
            return "resource/graphics/Com/Wui/Framework/Gui/ForbiddenIcon.ico";
        }

        protected getPageTitle() : string {
            return "WUI - HTTP 401";
        }

        protected getMessageHeader() : string {
            return "HTTP status 401";
        }

        protected getMessageBody() : string {
            return "Not authorized.";
        }

        protected setExpirationTime($value : string) : void {
            this.tokenExpiration = Property.String(this.tokenExpiration, $value);
        }

        protected reinvokeRequest($token : string) : void {
            if (this.RequestArgs().POST().KeyExists("Protocol")) {
                const protocol : IWebServiceProtocol = this.RequestArgs().POST().getItem("Protocol");
                const client : IWebServiceClient = this.RequestArgs().POST().getItem("Client");
                protocol.origin = this.getRequest().getBaseUrl();
                protocol.status = HttpStatusType.CONTINUE;
                protocol.data = ObjectDecoder.Base64(ObjectDecoder.Base64(<string>protocol.data));

                if (!ObjectValidator.IsEmptyOrNull(client)) {
                    const persistence : IPersistenceHandler =
                        PersistenceFactory.getPersistence(PersistenceType.CLIENT_IP, this.getClassName());
                    persistence.ExpireTime(this.tokenExpiration);
                    persistence.Variable("token", $token);
                    client.Send(<any>JSON.stringify(protocol));
                }
            }
        }
    }
}
