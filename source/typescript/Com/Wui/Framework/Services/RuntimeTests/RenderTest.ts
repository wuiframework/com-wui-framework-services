/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Services.RuntimeTests {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;

    export class RenderTest extends Com.Wui.Framework.Gui.HttpProcessor.Resolvers.RuntimeTestRunner {

        public CSSTest() : void {
            Echo.Println("" +
                "<div id=\"CssTestTarget\" style=\"width: 200px; height: 50px; border: 1px solid blue;\">" +
                "Test something on me..." +
                "</div>");
            this.addButton("Blur Filter", () : void => {
                if (this.getRequest().IsWuiJre()) {
                    // under Chrome version 61 a prefix -webkit- is REQUIRED. (we use Chrome 51.x)
                    ElementManager.setCssProperty("CssTestTarget", "-webkit-filter", "blur(4px)");
                } else {
                    ElementManager.setCssProperty("CssTestTarget", "filter", "blur(4px)");
                }
            });
        }
    }
}
/* dev:end */
