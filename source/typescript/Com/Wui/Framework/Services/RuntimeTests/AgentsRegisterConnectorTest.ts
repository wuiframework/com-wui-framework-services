/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Services.RuntimeTests {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;
    import WebServiceClientFactory = Com.Wui.Framework.Services.WebServiceApi.WebServiceClientFactory;
    import IWebServiceClient = Com.Wui.Framework.Services.Interfaces.IWebServiceClient;
    import AgentsRegisterConnector = Com.Wui.Framework.Services.Connectors.AgentsRegisterConnector;
    import IAgentInfo = Com.Wui.Framework.Services.Connectors.IAgentInfo;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;

    export class AgentsRegisterConnectorTest extends Com.Wui.Framework.Gui.HttpProcessor.Resolvers.RuntimeTestRunner {

        private connector : AgentsRegisterConnector;

        public before() : void {
            this.connector = new AgentsRegisterConnector();
            // this.connector = new AgentsRegisterConnector(true, "http://192.168.99.1:80/connector.config.jsonp");

            this.connector.getEvents().OnError(($args : ErrorEventArgs) : void => {
                Echo.Println($args.Message());
            });

            const client : IWebServiceClient = WebServiceClientFactory.getClientById(this.connector.getId());
            client.getServerPath(($path : string) : void => {
                Echo.Printf("<b>Hub configuration:</b> uri - \"{0}\", server root - \"{1}\"", client.getServerUrl(), $path);
            });
        }

        public AgentTest() : void {
            this.addButton("getAgentsList", () : void => {
                this.connector.getAgentsList()
                    .Then(($list : IAgentInfo[]) : void => {
                        Echo.Printf(JSON.stringify($list, null, 2));
                    });
            });

            this.addButton("RegisterAgent", () : void => {
                this.connector.RegisterAgent({name: "test-connector", platform: "unknown", domain: "none", version: "newest"})
                    .OnMessage(($data : any) : void => {
                        Echo.Printf("Message data: " + JSON.stringify($data));
                    })
                    .Then(($success : boolean) : void => {
                        if ($success) {
                            Echo.Printf("Registration success");
                        } else {
                            Echo.Printf("Registration failed");
                        }
                    });
            });

            this.addButton("ForwardMessage", () : void => {
                this.connector.ForwardMessage(StringUtils.getSha1("none" + "unknown"), "some data")
                    .Then(($status : boolean) : void => {
                        Echo.Printf("Forward message status: " + $status);
                    });
            });
        }
    }
}
/* dev:end */
