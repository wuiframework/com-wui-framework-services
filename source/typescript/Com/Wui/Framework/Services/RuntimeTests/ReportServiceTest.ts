/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Services.RuntimeTests {
    "use strict";
    import ReportServiceConnector = Com.Wui.Framework.Services.Connectors.ReportServiceConnector;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import EnvironmentArgs = Com.Wui.Framework.Commons.EnvironmentArgs;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ImageTransform = Com.Wui.Framework.Gui.ImageProcessor.ImageTransform;
    import IReportProtocol = Com.Wui.Framework.Services.Connectors.IReportProtocol;

    export class ReportServiceTest extends Com.Wui.Framework.Gui.HttpProcessor.Resolvers.RuntimeTestRunner {
        private service : ReportServiceConnector;

        public testLogItService() : void {
            let sendCounter : number = 0;
            this.addButton("Transfer log message", () : void => {
                this.service
                    .LogIt("Data send from test code. Current session send count: {0}", sendCounter)
                    .Then(() : void => {
                        Echo.Printf("Data has been sent.");
                        sendCounter++;
                    });
            });
        }

        public testSendMailService() : void {
            this.addButton("Send test mail", () : void => {
                this.service
                    .SendMail("jakub.cieslar@nxp.com", "Test of send mail service",
                        "<html><body>" +
                        "<h2 style='font-style:italic; color:brown;'>HTML text</h2>" +
                        "<a href='http://www.nxp.com' target='_blank'>Link to NXP</a>" +
                        "</body></html>",
                        [
                            "./test/resource/data/testFile1.txt",
                            "com-wui-framework-rest-services/test/resource/data/testFile2.txt"// ,
                            // "C:/Users/B57180/AppData/Local/Apps/WUIFramework/WUILocalhost/www/" +
                            // "com-wui-framework-rest-services/test/resource/data/testFile3.txt"
                        ],
                        "plain text, utf-8 test: +++ ;ěščřžýáíé=°\"ů")
                    .Then(($success : boolean) : void => {
                        if ($success) {
                            Echo.Printf("Email has been sent successfully.");
                        } else {
                            Echo.Printf("Email send has failed.");
                        }
                    });
            });
        }

        public testCrashReportService() : void {
            const args : EnvironmentArgs = this.getEnvironmentArgs();
            let appLog : string = "";
            LogIt.setOnPrint(($message : string) : void => {
                appLog += $message;
            });
            LogIt.Info("Initialization of ReportServiceTest Unit test");
            const printScreen : HTMLImageElement = document.createElement("img");
            printScreen.src = "test/resource/data/Com/Wui/Framework/Services/PrintScreen.png";
            this.addButton("Create report", () : void => {
                Echo.Println("Sending report .");
                const timeout : any = setInterval(() : void => {
                    Echo.Print(".");
                }, 250);
                LogIt.Info("Preparation for initialization of report service");
                this.service.getEvents().OnStart(() : void => {
                    LogIt.Debug("Report services has been initialized");
                });
                const EOL : string = StringUtils.NewLine(false);
                const data : IReportProtocol = {
                    appId      : StringUtils.getSha1(args.getAppName() + args.getProjectVersion()),
                    appName    : args.getAppName(),
                    appVersion : args.getProjectVersion(),
                    log        : appLog,
                    printScreen: ImageTransform.getStream(
                        ImageTransform.Resize(ImageTransform.ToCanvas(printScreen), 500, 500, false), true),
                    timeStamp  : new Date().getTime(),
                    trace      : "" +
                        "ExceptionsManagerTest.prototype.testSetEvent@file:///D:/__PROJECTS__/WUI/Source/com-wui-framework-" +
                        "commons/build/target/resource/javascript/com-wui-framework-commons-1-1-0-beta.min.js:10553:71 " + EOL +
                        "RuntimeTestRunner.prototype.resolver@file:///D:/__PROJECTS__/WUI/Source/com-wui-framework-commons/" +
                        "build/target/resource/javascript/com-wui-framework-commons-1-1-0-beta.min.js:7649:37 " + EOL +
                        "BaseHttpResolver.prototype.Process@file:///D:/__PROJECTS__/WUI/Source/com-wui-framework-commons/build" +
                        "/target/resource/javascript/com-wui-framework-commons-1-1-0-beta.min.js:7481:37 " + EOL +
                        "HttpResolver.prototype.ResolveRequest@file:///D:/__PROJECTS__/WUI/Source/com-wui-framework-commons/" +
                        "build/target/resource/javascript/com-wui-framework-commons-1-1-0-beta.min.js:11380:37 " + EOL +
                        "EventsManager.prototype.FireEvent/:<@file:///D:/__PROJECTS__/WUI/Source/com-wui-framework-commons/" +
                        "build/target/resource/javascript/com-wui-framework-commons-1-1-0-beta.min.js:5352:53"
                };
                this.service
                    .CreateReport(data)
                    .Then(($success : boolean) : void => {
                        clearInterval(timeout);
                        if ($success) {
                            Echo.Printf("Report has been sent successfully.");
                        } else {
                            Echo.Printf("Report send has failed.");
                        }
                    });
            });
        }

        public testTimeout() : void {
            const service : ReportServiceConnector = new ReportServiceConnector(false,
                "test/resource/data/Com/Wui/Framework/Services/RuntimeTests/timeout.connector.config.jsonp");
            this.addButton("Test connection", () : void => {
                service.LogIt("Data send from test code.");
            });
        }

        protected before() : void {
            this.service = new ReportServiceConnector(false,
                // "https://hub.dev.wuiframework.com/connector.config.jsonp"
                "https://localhost.wuiframework.com/connector.config.jsonp"
            );
        }
    }
}
/* dev:end */
