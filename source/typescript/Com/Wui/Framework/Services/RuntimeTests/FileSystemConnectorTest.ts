/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Services.RuntimeTests {
    "use strict";
    import FileSystemHandlerConnector = Com.Wui.Framework.Services.Connectors.FileSystemHandlerConnector;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IFileSystemItemProtocol = Com.Wui.Framework.Commons.Interfaces.IFileSystemItemProtocol;
    import ProgressEventArgs = Com.Wui.Framework.Commons.Events.Args.ProgressEventArgs;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import TimeoutManager = Com.Wui.Framework.Commons.Events.TimeoutManager;
    import FileSystemDownloadOptions = Com.Wui.Framework.Services.Connectors.FileSystemDownloadOptions;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ShortcutOptions = Com.Wui.Framework.Services.Connectors.ShortcutOptions;
    import ShortcutRunStyle = Com.Wui.Framework.Services.Connectors.ShortcutRunStyle;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;

    export class FileSystemConnectorTest extends Com.Wui.Framework.Gui.HttpProcessor.Resolvers.RuntimeTestRunner {

        private fileSystem : FileSystemHandlerConnector;
        private resourceData : string;

        public before() : void {
            this.fileSystem = new FileSystemHandlerConnector();
            this.fileSystem.getEvents().OnError(($args : ErrorEventArgs) : void => {
                Echo.Println($args.Message());
            });
            this.resourceData = this.getAbsoluteRoot() + "/test/resource/data/Com/Wui/Framework/Services/RuntimeTests";
        }

        public testExists() : void {
            this.addButton("File exists", () : void => {
                this.fileSystem
                    .Exists(this.resourceData + "/testCaseA.cmd")
                    .Then(($success : boolean) : void => {
                        if ($success) {
                            Echo.Printf("<b>File has been found</b>");
                        } else {
                            Echo.Printf("<b>File does not exist</b>");
                        }
                    });
            });
        }

        public testReadFile() : void {
            this.addButton("Read file", () : void => {
                this.fileSystem
                    .Read(this.resourceData + "/testCaseA.cmd")
                    .Then(($data : string) : void => {
                        Echo.Printf("<b>File content:</b>");
                        Echo.PrintCode($data);
                    });
            });
        }

        public testWriteFile() : void {
            this.addButton("Write file", () : void => {
                this.fileSystem
                    .Write(this.resourceData + "/test.txt", "some test data")
                    .Then(($success : boolean) : void => {
                        if ($success) {
                            Echo.Printf("<b>File has been written successfully</b>");
                            this.fileSystem
                                .Delete(this.resourceData + "/test.txt")
                                .Then(($success : boolean) : void => {
                                    Echo.Printf("<b>File has been deleted</b>");
                                });
                        } else {
                            Echo.Printf("<b>File write has failed</b>");
                        }
                    });
            });
            this.addButton("Write large file", () : void => {
                let data : string = "some long long long long long long long long long long long long long long long long test data\r\n";
                const manager : TimeoutManager = new TimeoutManager();
                const prepareData : any = () : void => {
                    if (StringUtils.Length(data) < 1024 * 1024 * 5) {
                        data += data;
                        manager.Add(prepareData);
                    } else {
                        this.fileSystem
                            .Write(this.resourceData + "/largeTest.txt", data)
                            .Then(($success : boolean) : void => {
                                if ($success) {
                                    Echo.Printf("<b>File has been written successfully</b>");
                                } else {
                                    Echo.Printf("<b>File write has failed</b>");
                                }
                            });
                    }
                };
                manager.Add(prepareData);
                manager.Execute();
            });
        }

        public testRenameFile() : void {
            this.addButton("Rename file", () : void => {
                this.fileSystem
                    .Rename(this.resourceData + "/testCaseA.cmd", this.resourceData + "/renamed.cmd")
                    .Then(($success : boolean) : void => {
                        if ($success) {
                            Echo.Printf("<b>File has been renamed successfully</b>");
                            this.fileSystem
                                .Rename(this.resourceData + "/renamed.cmd", this.resourceData + "/testCaseA.cmd")
                                .Then(($success : boolean) : void => {
                                    if ($success) {
                                        Echo.Printf("<b>File has been renamed back</b>");
                                    } else {
                                        Echo.Printf("<b>File renamed (back) has failed</b>");
                                    }
                                });
                        } else {
                            Echo.Printf("<b>File renamed has failed</b>");
                        }
                    });
            });
        }

        public testCopyFile() : void {
            this.addButton("Copy file", () : void => {
                this.fileSystem
                    .Copy(this.resourceData + "/archTest1.zip", this.resourceData + "/copiedArch.zip")
                    .Then(($success : boolean) : void => {
                        if ($success) {
                            Echo.Printf("<b>File has been copied successfully</b>");
                        } else {
                            Echo.Printf("<b>File copy has failed</b>");
                        }
                    });
            });
        }

        public testDeleteFile() : void {
            this.addButton("Delete file", () : void => {
                this.fileSystem.Exists(this.resourceData + "/copiedArch.zip").Then(($status : boolean) : void => {
                    if ($status) {
                        this.fileSystem
                            .Delete(this.resourceData + "/copiedArch.zip")
                            .Then(($success : boolean) : void => {
                                if ($success) {
                                    Echo.Printf("<b>File has been deleted successfully</b>");
                                } else {
                                    Echo.Printf("<b>File delete has failed</b>");
                                }
                            });
                    } else {
                        Echo.Printf("<b>Click on Copy file before.</b>");
                    }
                });
            });
        }

        public testGetTempPath() : void {
            this.addButton("Get Temp path location", () : void => {
                this.fileSystem
                    .getTempPath()
                    .Then(($data : string) : void => {
                        Echo.Printf("<b>Temp folder path:</b>");
                        Echo.PrintCode($data);
                    });
            });
        }

        public testCreateDirectory() : void {
            this.addButton("Create Dir", () : void => {
                this.fileSystem
                    .CreateDirectory(this.resourceData + "/TestFolder")
                    .Then(($success : boolean) : void => {
                        if ($success) {
                            Echo.Printf("<b>Folder has been created successfully</b>");
                            this.fileSystem
                                .Delete(this.resourceData + "/TestFolder")
                                .Then(($success : boolean) => {
                                    if ($success) {
                                        Echo.Printf("<b>Created folder has been deleted</b>");
                                    } else {
                                        Echo.Printf("<b>Created folder can not be deleted</b>");
                                    }
                                });
                        } else {
                            Echo.Printf("<b>Folder creation has failed</b>");
                        }
                    });
            });
        }

        public testDownload() : void {
            let connectionId : number;
            this.addButton("Download file", () : void => {
                this.fileSystem
                    .Download("https://bitbucket.org/wuiframework/com-wui-framework-builder/" +
                        "raw/f897ba2642a2a6512e8b2bbf1f0b44bfee75a474/README.md")
                    .OnStart(($id : number) : void => {
                        connectionId = $id;
                        Echo.Printf("start");
                    })
                    .OnChange(($args : ProgressEventArgs) : void => {
                        Echo.Printf("change: {0}/{1}", $args.CurrentValue() + "", $args.RangeEnd() + "");
                    })
                    .OnComplete(($tmpPath : string) : void => {
                        Echo.Printf("complete: {0}", $tmpPath);
                    });
            });

            this.addButton("Stop download", () : void => {
                this.fileSystem
                    .AbortDownload(connectionId)
                    .Then(($status : boolean) : void => {
                        if ($status) {
                            Echo.Printf("Download has been aborted.");
                        } else {
                            Echo.Printf("Download abort has been failed.");
                        }
                    });
            });

            this.addButton("Download as stream", () : void => {
                this.fileSystem
                    .Download(<FileSystemDownloadOptions>{
                        headers     : {
                            "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0"
                        },
                        streamOutput: true,
                        url         : "https://bitbucket.org/wuiframework/com-wui-framework-builder/" +
                            "raw/f897ba2642a2a6512e8b2bbf1f0b44bfee75a474/README.md"
                    })
                    .OnStart(() : void => {
                        Echo.Printf("start");
                    })
                    .OnChange(($args : ProgressEventArgs) : void => {
                        Echo.Printf("change: {0}/{1}", $args.CurrentValue() + "", $args.RangeEnd() + "");
                    })
                    .OnComplete(($headers : ArrayList<string>, $body : string) : void => {
                        Echo.Printf($headers);
                        Echo.PrintCode($body);
                    });
            });

            this.addButton("Download with error handler", () : void => {
                this.fileSystem
                    .Download(<FileSystemDownloadOptions>{
                        url: "https://bitbucket.org/unknown-for-test/com-wui-framework-builder/" +
                            "raw/f897ba2642a2a6512e8b2bbf1f0b44bfee75a474/README.md"
                    })
                    .OnError(($error : ErrorEventArgs) : void => {
                        Echo.Printf("download error handler called with: {0}", $error);
                    })
                    .OnComplete(() : void => {
                        Echo.Printf("<b>Failed to call error handler for Download</b>");
                    });
            });
        }

        public testUnpack() : void {
            this.addButton("Unpack zip file", () : void => {
                this.fileSystem
                    .Unpack(this.resourceData + "/archTest1.zip")
                    .Then(($path : string) : void => {
                        if (!ObjectValidator.IsEmptyOrNull($path)) {
                            Echo.Printf("<b>File unpacked to:</b>");
                            Echo.Printf($path);
                        }
                    });
            });
            this.addButton("Unpack tar.bz2 file", () : void => {
                this.fileSystem
                    .Unpack(this.resourceData + "/archTest2.tar.bz2")
                    .Then(($path : string) : void => {
                        if (!ObjectValidator.IsEmptyOrNull($path)) {
                            Echo.Printf("<b>File unpacked to:</b>");
                            Echo.Printf($path);
                        }
                    });
            });
            this.addButton("Unpack with error (hard to simulate, delete lzma-native.dll from connector/builder before)",
                () : void => {
                    this.fileSystem
                        .Unpack("*//*/*-+*$unknown")
                        .OnError(($error : ErrorEventArgs) : void => {
                            Echo.Printf("Error handler called for Unpack with: {0}", $error);
                        })
                        .Then(($path : string) : void => {
                            Echo.Printf("Failed to handle error for Unpack");
                        });
                });
        }

        public testPack() : void {
            this.addButton("Pack zip file", () : void => {
                this.fileSystem
                    .Pack(this.resourceData + "/archTest", {type: "zip"})
                    .Then(($path : string) : void => {
                        if (!ObjectValidator.IsEmptyOrNull($path)) {
                            Echo.Printf("<b>File packed to:</b>");
                            Echo.Printf($path);
                        }
                    });
            });
            this.addButton("Pack tar.bz2 file", () : void => {
                this.fileSystem
                    .Pack(this.resourceData + "/archTest", {type: "tar.bz2"})
                    .Then(($path : string) : void => {
                        if (!ObjectValidator.IsEmptyOrNull($path)) {
                            Echo.Printf("<b>File packed to:</b>");
                            Echo.Printf($path);
                        }
                    });
            });
            this.addButton("Pack with error", () : void => {
                this.fileSystem
                    .Pack(this.resourceData + "/unknown")
                    .OnError(($error : ErrorEventArgs) : void => {
                        Echo.Printf("Error handler called for Unpack with: {0}", $error);
                    })
                    .Then(($path : string) : void => {
                        Echo.Printf("Failed to handle error for Unpack");
                    });
            });
        }

        public testCopyDir() : void {
            let barIterator : number = 0;
            this.addButton("Copy dir", () : void => {
                barIterator++;
                Echo.Println("Directory copy progress: <span id=\"testCopyDirBar_" + barIterator + "\"></span>");
                this.fileSystem
                    .Copy(this.resourceData, this.resourceData + "_Copy")
                    .OnChange(($args : ProgressEventArgs) : void => {
                        ElementManager.setInnerHtml("testCopyDirBar_" + barIterator,
                            $args.CurrentValue() + "/" + $args.RangeEnd());
                    })
                    .Then(($success : boolean) : void => {
                        if ($success) {
                            Echo.Printf("<b>Directory has been copied successfully</b>");
                        } else {
                            Echo.Printf("<b>Directory copy has failed</b>");
                        }
                    });
            });
        }

        public testDeleteDir() : void {
            let barIterator : number = 0;
            this.addButton("Delete dir", () : void => {
                barIterator++;
                Echo.Println("Directory delete progress: <span id=\"testDeleteDirBar_" + barIterator + "\"></span>");
                this.fileSystem
                    .Delete(this.resourceData + "_Copy")
                    .OnChange(($args : ProgressEventArgs) : void => {
                        ElementManager.setInnerHtml("testDeleteDirBar_" + barIterator,
                            $args.CurrentValue() + "/" + $args.RangeEnd());
                    })
                    .Then(($success : boolean) : void => {
                        if ($success) {
                            Echo.Printf("<b>Directory has been deleted successfully</b>");
                        } else {
                            Echo.Printf("<b>Directory deleted has failed</b>");
                        }
                    });
            });
        }

        public testCreateShortcut() : void {
            this.addButton("Create shortcut", () : void => {
                this.fileSystem.CreateShortcut("source", "destination", <ShortcutOptions>{
                    args      : ["arg1", "arg2"],
                    desc      : "descritpion",
                    hotkey    : 5,
                    icon      : "path-to-icon",
                    iconIndex : 3,
                    runStyle  : ShortcutRunStyle.NORMAL,
                    workingDir: "working directory"
                })
                    .Then(($code : boolean) : void => {
                        if ($code === true) {
                            Echo.Println("succeed");
                        } else {
                            Echo.Println("failed");
                        }
                    });
            });

            this.addButton("Create shortcut with error", () : void => {
                this.fileSystem.CreateShortcut("", "", <ShortcutOptions>{
                    args      : ["arg1", "arg2"],
                    desc      : "descritpion",
                    hotkey    : 5,
                    icon      : "path-to-icon",
                    iconIndex : 3,
                    runStyle  : ShortcutRunStyle.NORMAL,
                    workingDir: "working directory"
                })
                    .OnError(($error : ErrorEventArgs) : void => {
                        Echo.Printf("Error handler called for Create shortcut with: {0}", $error);
                    })
                    .Then(($code : boolean) : void => {
                        Echo.Printf("Failed to handle error for Create shortcut.");
                    });
            });
        }

        public testgetLinkTargetPath() : void {
            this.addButton("Get Link target", () : void => {
                this.fileSystem.getLinkTargetPath("D:/SystemTemp")
                    .Then(($path : string) : void => {
                        if (!ObjectValidator.IsEmptyOrNull($path)) {
                            Echo.Println("real path: " + $path);
                        } else {
                            Echo.Println("failed");
                        }
                    });
            });
        }

        public testGetNetworkMap() : void {
            this.addButton("Get network map", () : void => {
                this.fileSystem
                    .getNetworkMap()
                    .Then(($data : IFileSystemItemProtocol[]) : void => {
                        Echo.Printf($data);
                    });
            });

            this.addButton("Get network map advanced", () : void => {
                this.fileSystem
                    .getNetworkMap(true)
                    .Then(($data : IFileSystemItemProtocol[]) : void => {
                        Echo.Printf($data);
                    });
            });
        }

        public testGetRootFolders() : void {
            this.addButton("Get root FS interface", () : void => {
                this.fileSystem
                    .getPathMap()
                    .Then(($data : IFileSystemItemProtocol[]) : void => {
                        Echo.Printf($data);
                    });
            });
        }

        public testGetDirectoryContent() : void {
            this.addButton("Get folder content", () : void => {
                this.fileSystem
                    .getDirectoryContent("C:\\Windows")
                    .Then(($data : IFileSystemItemProtocol[]) : void => {
                        Echo.Printf($data);
                    });
            });
        }

        public testGetPathMap() : void {
            this.addButton("Get path map", () : void => {
                this.fileSystem
                    .getPathMap("C:\\Program Files\\")
                    .Then(($data : IFileSystemItemProtocol[]) : void => {
                        Echo.Printf($data);
                    });
            });
        }

        public testExpand() : void {
            this.addButton("Expand simple", () : void => {
                this.fileSystem
                    .Expand("C:/Windows/*")
                    .Then(($data : string[]) : void => {
                        Echo.Printf($data);
                    });
            });
            this.addButton("Expand map", () : void => {
                this.fileSystem
                    .Expand(["C:/Program Files/*", "C:/Windows/*"])
                    .Then(($data : string[]) : void => {
                        Echo.Printf($data);
                    });
            });
        }

        public AgentTest() : void {
            const connector : FileSystemHandlerConnector = new FileSystemHandlerConnector();
            connector.setAgent("test-connector-win", "https://localhost.wuiframework.com/connector.config.jsonp");

            this.addButton("Forward to agent", () : void => {
                connector
                    .Exists(this.getAbsoluteRoot())
                    .Then(($success : boolean) : void => {
                        Echo.Printf($success);
                    });
            });
        }
    }
}
/* dev:end */
