/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Services.RuntimeTests {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import BasePageController = Com.Wui.Framework.Services.HttpProcessor.Resolvers.BasePageController;
    import BasePanelViewer = Com.Wui.Framework.Gui.Primitives.BasePanelViewer;
    import BasePanel = Com.Wui.Framework.Gui.Primitives.BasePanel;
    import ResizeableType = Com.Wui.Framework.Gui.Enums.ResizeableType;
    import FormsObject = Com.Wui.Framework.Gui.Primitives.FormsObject;
    import IGuiElement = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiElement;
    import IButton = Com.Wui.Framework.Gui.Interfaces.UserControls.IButton;
    import GuiCommons = Com.Wui.Framework.Gui.Primitives.GuiCommons;
    import IResizeBar = Com.Wui.Framework.Gui.Interfaces.Components.IResizeBar;
    import IResizeBarEvents = Com.Wui.Framework.Gui.Interfaces.Events.IResizeBarEvents;
    import IButtonEvents = Com.Wui.Framework.Gui.Interfaces.Events.IButtonEvents;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import GuiObjectManager = Com.Wui.Framework.Gui.GuiObjectManager;
    import EventType = Com.Wui.Framework.Gui.Enums.Events.EventType;
    import GeneralEventOwner = Com.Wui.Framework.Gui.Enums.Events.GeneralEventOwner;
    import MoveEventArgs = Com.Wui.Framework.Gui.Events.Args.MoveEventArgs;
    import IGuiCommons = Com.Wui.Framework.Gui.Interfaces.Primitives.IGuiCommons;
    import ResizeBarEventArgs = Com.Wui.Framework.Gui.Events.Args.ResizeBarEventArgs;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import IDragBar = Com.Wui.Framework.Gui.Interfaces.Components.IDragBar;
    import IDragBarEvents = Com.Wui.Framework.Gui.Interfaces.Events.IDragBarEvents;

    export class MockButton extends FormsObject implements IButton {
        private buttonType : string;

        constructor($buttonType? : any, $id? : string) {
            super($id);
            this.GuiType($buttonType);
        }

        public GuiType($buttonType? : any) : any {
            if (ObjectValidator.IsSet($buttonType)) {
                this.buttonType = $buttonType;
            }
            return this.buttonType;
        }

        public Text($value? : string) : string {
            return "";
        }

        public getEvents() : IButtonEvents {
            return <IButtonEvents>super.getEvents();
        }

        public IconName($value? : any) : any {
            return "";
        }

        public IsSelected($value? : boolean) : boolean {
            return false;
        }

        protected innerHtml() : IGuiElement {
            return this.addElement(this.Id() + "_Type").StyleClassName(this.buttonType);
        }

        protected innerCode() : IGuiElement {
            this.getEvents().Subscriber(this.Id() + "_Type");

            return super.innerCode();
        }

        protected cssContainerName() : string {
            return "Button";
        }
    }

    export class MockResizeBar extends GuiCommons implements IResizeBar {
        private resizeType : ResizeableType;

        public static setWidth($element : MockResizeBar, $value : number) : void {
            ElementManager.setWidth($element.Id() + "_Position", $value);
        }

        public static setHeight($element : MockResizeBar, $value : number) : void {
            ElementManager.setHeight($element.Id() + "_Position", $value);
        }

        constructor($resizeableType? : ResizeableType, $id? : string) {
            super($id);
            this.ResizeableType($resizeableType);
        }

        public ResizeableType($type? : ResizeableType) : ResizeableType {
            if (ObjectValidator.IsSet($type)) {
                this.resizeType = $type;
            }
            return this.resizeType;
        }

        public getEvents() : IResizeBarEvents {
            return <IResizeBarEvents>super.getEvents();
        }

        protected innerCode() : IGuiElement {
            this.getEvents().Subscriber(this.Id() + "_Position");

            this.getEventsManager().setEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_START,
                ($args : MoveEventArgs, $manager : GuiObjectManager) : void => {
                    this.getEvents().FireAsynchronousMethod(() : void => {
                        if ($manager.IsActive(MockResizeBar)) {
                            $args.StopAllPropagation();
                            const element : MockResizeBar = <MockResizeBar>$manager.getActive(MockResizeBar).getFirst();
                            const eventArgs : ResizeBarEventArgs = new ResizeBarEventArgs();
                            eventArgs.Owner(element);
                            eventArgs.ResizeableType(element.ResizeableType());
                            this.getEventsManager().FireEvent(element, EventType.ON_RESIZE_START, eventArgs, false);
                        }
                    });
                });

            this.getEventsManager().setEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_CHANGE,
                ($args : MoveEventArgs, $manager : GuiObjectManager) : void => {
                    this.getEvents().FireAsynchronousMethod(() : void => {
                        if ($manager.IsActive(MockResizeBar)) {
                            $args.StopAllPropagation();
                            const element : MockResizeBar = <MockResizeBar>$manager.getActive(MockResizeBar).getFirst();
                            switch (element.ResizeableType()) {
                            case ResizeableType.HORIZONTAL_AND_VERTICAL:
                                document.body.style.cursor = "nw-resize";
                                break;
                            case ResizeableType.HORIZONTAL:
                                document.body.style.cursor = "w-resize";
                                break;
                            case ResizeableType.VERTICAL:
                                document.body.style.cursor = "n-resize";
                                break;
                            default :
                                document.body.style.cursor = "default";
                                break;
                            }

                            let distanceX : number = 0;
                            let distanceY : number = 0;

                            if (element.ResizeableType() === ResizeableType.HORIZONTAL_AND_VERTICAL ||
                                element.ResizeableType() === ResizeableType.HORIZONTAL) {
                                distanceX = $args.getDistanceX();
                            }
                            if (element.ResizeableType() === ResizeableType.HORIZONTAL_AND_VERTICAL ||
                                element.ResizeableType() === ResizeableType.VERTICAL) {
                                distanceY = $args.getDistanceY();
                            }

                            const eventArgs : ResizeBarEventArgs = new ResizeBarEventArgs();
                            eventArgs.Owner(element);
                            eventArgs.ResizeableType(element.ResizeableType());
                            eventArgs.DistanceX(distanceX);
                            eventArgs.DistanceY(distanceY);
                            this.getEventsManager().FireEvent(element, EventType.ON_RESIZE_CHANGE, eventArgs, false);
                        }
                    });
                });

            this.getEventsManager().setEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_COMPLETE,
                ($args : MoveEventArgs, $manager : GuiObjectManager) : void => {
                    this.getEvents().FireAsynchronousMethod(() : void => {
                        $args.StopAllPropagation();
                        const elements : ArrayList<IGuiCommons> = $manager.getActive(<ClassName>MockResizeBar);
                        elements.foreach(($element : MockResizeBar) : void => {
                            const eventArgs : ResizeBarEventArgs = new ResizeBarEventArgs();
                            eventArgs.Owner($element);
                            eventArgs.ResizeableType($element.ResizeableType());
                            this.getEventsManager().FireEvent($element, EventType.ON_RESIZE_COMPLETE, eventArgs, false);
                            this.getEventsManager().FireEvent($element, EventType.ON_RESIZE, eventArgs, false);
                            $manager.setActive($element, false);
                        });
                    });
                });

            this.getEvents().setOnMouseDown(($args : MoveEventArgs, $manager : GuiObjectManager) : void => {
                $manager.setActive(this, true);
            });

            this.getEvents().setOnMouseUp(() : void => {
                document.body.style.cursor = "default";
            });

            return super.innerCode();
        }

        protected innerHtml() : IGuiElement {
            return this.addElement().Add(this.addElement(this.Id() + "_Position").StyleClassName(this.resizeType));
        }

        protected cssContainerName() : string {
            return "ResizeBar";
        }
    }

    export class MockDragBar extends GuiCommons implements IDragBar {

        public getEvents() : IDragBarEvents {
            return <IDragBarEvents>super.getEvents();
        }

        protected innerCode() : IGuiElement {
            this.getEventsManager().setEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_START,
                ($args : MoveEventArgs, $manager : GuiObjectManager) : void => {
                    this.getEvents().FireAsynchronousMethod(() : void => {
                        if ($manager.IsActive(MockDragBar)) {
                            $args.StopAllPropagation();
                            const element : MockDragBar = <MockDragBar>$manager.getActive(MockDragBar).getFirst();
                            $args.Owner(element);
                            this.getEventsManager().FireEvent(element, EventType.ON_DRAG_START, $args, false);
                            document.body.style.cursor = "move";
                        }
                    });
                });

            this.getEventsManager().setEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_CHANGE,
                ($args : MoveEventArgs, $manager : GuiObjectManager) : void => {
                    this.getEvents().FireAsynchronousMethod(() : void => {
                        if ($manager.IsActive(MockDragBar)) {
                            $args.StopAllPropagation();
                            const element : MockDragBar = <MockDragBar>$manager.getActive(MockDragBar).getFirst();
                            $args.Owner(element);
                            this.getEventsManager().FireEvent(element, EventType.ON_DRAG_CHANGE, $args);
                        }
                    });
                });

            this.getEventsManager().setEvent(GeneralEventOwner.MOUSE_MOVE, EventType.ON_COMPLETE,
                ($args : MoveEventArgs, $manager : GuiObjectManager) : void => {
                    this.getEvents().FireAsynchronousMethod(() : void => {
                        $args.StopAllPropagation();
                        const elements : ArrayList<IGuiCommons> = $manager.getActive(<ClassName>MockDragBar);
                        elements.foreach(($element : MockDragBar) : void => {
                            $args.Owner($element);
                            this.getEventsManager().FireEvent($element, EventType.ON_DRAG_COMPLETE, $args, false);
                            $manager.setActive($element, false);
                        });
                    });
                });

            this.getEvents().setOnMouseDown(($args : MoveEventArgs, $manager : GuiObjectManager) : void => {
                $manager.setActive(this, true);
            });

            this.getEvents().setOnMouseUp(() : void => {
                document.body.style.cursor = "default";
            });

            return super.innerCode();
        }

        protected cssContainerName() : string {
            return "DragBar";
        }
    }

    export class MockPanel extends BasePanel {

        protected innerHtml() : IGuiElement {
            return this.addElement()
                .Add(this.addElement().Width(500).Height(500)
                    .setAttribute("background-color", "silver")
                    .setAttribute("position", "absolute")
                    .Add("validation element with size: 500x500 px")
                )
                .Add(this.addElement().StyleClassName("Logo")
                    .Add(this.addElement().StyleClassName("WUI"))
                );
        }
    }

    export class MockViewer extends BasePanelViewer {
        constructor() {
            super();
            this.setInstance(new MockPanel());
        }
    }

    export class PageControllerTest extends BasePageController {

        constructor() {
            super();

            this.setModelClassName(MockViewer);

            this.minimizeButton.GuiType("AppMinimize");
            this.maximizeButton.GuiType("AppMaximize");
            this.closeButton.GuiType("AppClose");
            this.setResizeableType(ResizeableType.HORIZONTAL_AND_VERTICAL);
        }

        protected getAppImageButtonClass() : any {
            return MockButton;
        }

        protected getAppResizeBarClass() : any {
            return MockResizeBar;
        }

        protected getAppDragBarClass() : any {
            return MockDragBar;
        }

        protected resolver() : void {
            Echo.Println("<style>" +
                ".AppDragBar {cursor: move;}" +
                ".AppButtons {top: 5px !important; right: 5px !important;}" +
                ".Button {float: left; margin-left: 3px;}" +
                ".Button .AppMinimize {border-radius: 10px; background-color: green; width: 20px; height: 20px;}" +
                ".Button .AppMaximize {border-radius: 10px; background-color: orange; width: 20px; height: 20px;}" +
                ".Button .AppClose {border-radius: 10px; background-color: red; width: 20px; height: 20px;}" +
                ".ResizeBar {float: right; left: 0; top: 1px; position: relative;}" +
                ".ResizeBar .X {cursor: w-resize;}" +
                ".ResizeBar .Y {cursor: n-resize;}" +
                ".ResizeBar .XY {cursor: nw-resize;}" +
                ".MockPanel {background-color: gainsboro;}" +
                "</style>");
            super.resolver();
        }
    }
}
/* dev:end */
