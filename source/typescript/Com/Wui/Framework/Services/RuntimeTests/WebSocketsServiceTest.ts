/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Services.RuntimeTests {
    "use strict";
    import LiveContentWrapper = Com.Wui.Framework.Services.WebServiceApi.LiveContentWrapper;
    import IWebServiceClient = Com.Wui.Framework.Services.Interfaces.IWebServiceClient;
    import WebServiceClientFactory = Com.Wui.Framework.Services.WebServiceApi.WebServiceClientFactory;
    import WebServiceClientType = Com.Wui.Framework.Services.Enums.WebServiceClientType;
    import IWebServiceProtocol = Com.Wui.Framework.Services.Interfaces.IWebServiceProtocol;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;

    export class WebSocketsServiceTest extends Com.Wui.Framework.Gui.HttpProcessor.Resolvers.RuntimeTestRunner {

        public testSocketsConnection() : void {
            const service : IWebServiceClient = WebServiceClientFactory.getClient(WebServiceClientType.WUI_DESKTOP_CONNECTOR);

            service.getEvents().OnStart(() : void => {
                Echo.Printf("Connection has been started.");
            });
            service.getEvents().OnClose(() : void => {
                Echo.Printf("Connection has been closed.");
            });
            service.getEvents().OnTimeout(() : void => {
                Echo.Printf("Connection timeout has been reached.");
            });
            service.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
                ExceptionsManager.Throw($eventArgs.Owner().getClassName(), $eventArgs.Exception());
            });

            let sendCounter : number = 0;
            this.addButton("Send data to WebSocket service", () : void => {
                let receiveCounter : number = 0;
                for (let index : number = 0; index < 5; index++) {
                    const wrapper : LiveContentWrapper = new LiveContentWrapper("Com.Wui.Framework.Connector.Commons.LogIt");
                    service.Send(wrapper.getProtocolForInvokeMethod(
                        service.getId(), "Info", "Data send from test code. Current session send count: " + sendCounter),
                        ($dataObject : IWebServiceProtocol) : void => {
                            receiveCounter++;
                            Echo.Printf("<b>server acknowledge[" + receiveCounter + "] from " + $dataObject.origin + "</b>");
                        });
                    sendCounter++;
                }
            });

            this.addButton("Close connection", () : void => {
                service.StopCommunication();
            });
        }
    }
}
/* dev:end */
