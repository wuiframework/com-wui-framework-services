/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Services.RuntimeTests {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import InstallationRecipeDAO = Com.Wui.Framework.Services.DAO.InstallationRecipeDAO;
    import LanguageType = Com.Wui.Framework.Commons.Enums.LanguageType;
    import InstallationProtocolType = Com.Wui.Framework.Services.Enums.InstallationProtocolType;
    import InstallationProgressEventArgs = Com.Wui.Framework.Services.Events.Args.InstallationProgressEventArgs;
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import WindowManager = Com.Wui.Framework.Gui.Utils.WindowManager;
    import InstallationProgressCode = Com.Wui.Framework.Services.Enums.InstallationProgressCode;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;
    import EnvironmentArgs = Com.Wui.Framework.Commons.EnvironmentArgs;
    import IRuntimeTestPromise = Com.Wui.Framework.Commons.HttpProcessor.Resolvers.IRuntimeTestPromise;

    export class InstallationProcessTest extends Com.Wui.Framework.Gui.HttpProcessor.Resolvers.RuntimeTestRunner {
        private installDao : InstallationRecipeDAO;
        private installScript : string;
        private forceChain : boolean;
        private chain : string[];

        constructor() {
            super();
            this.setScript("test/resource/data/Com/Wui/Framework/Services/Configuration/BaseInstallationRecipe.jsonp");
            this.setChain([
                "Netfx4Full",
                "VisualStudio2015",
                "VisualCppBuildTools",
                "VCRedist2012",
                "VCRedist2015",
                "Java",
                "Git",
                "Python",
                "Ruby",
                "Cmake",
                "Mingw",
                "Msys2",
                "PortableGit",
                "PortableCMake"
            ]);
        }

        public testRegisterApp() : void {
            this.addButton("Add to WIN Register", () : void => {
                const env : EnvironmentArgs = this.getEnvironmentArgs();
                this.getDao().getConfigurationInstance().utils.RegisterApp(
                    true,
                    "WUIFramework/" + env.getAppName() + "_TEST",
                    env.getProjectVersion(),
                    StringUtils.Replace("C:/" +
                        env.getProjectName() + "-" + StringUtils.Replace(env.getProjectVersion(), ".", "-"), "/", "\\"),
                    ($status : boolean) : void => {
                        this.assertEquals($status, true, "Validate that WIN register has been added");
                        if ($status) {
                            this.getDao().getConfigurationInstance().utils.RegisterValueExists(
                                "HKLM/SOFTWARE/WUIFramework/" + env.getAppName() + "_TEST", "CurrentVersion",
                                ($status : boolean) : void => {
                                    this.assertEquals($status, true, "Validate existence of WIN register key");
                                    if ($status) {
                                        this.getDao().getConfigurationInstance().utils.RemoveRegisterKey(
                                            "HKLM/SOFTWARE/WUIFramework/" + env.getAppName() + "_TEST", "",
                                            ($status : boolean) : void => {
                                                this.assertEquals($status, true, "Validate that WIN register has been removed");
                                                if ($status) {
                                                    this.getDao().getConfigurationInstance().utils.RegisterValueExists(
                                                        "HKLM/SOFTWARE/WUIFramework/" + env.getAppName() + "_TEST",
                                                        "CurrentVersion",
                                                        ($status : boolean) : void => {
                                                            this.assertEquals($status, false, "Validate non-existence of WIN register key");
                                                        });
                                                }
                                            });
                                    }
                                });
                        }
                    });
            });

            this.addButton("Add to WIN Path", () : void => {
                const env : EnvironmentArgs = this.getEnvironmentArgs();
                const path : string = StringUtils.Replace("C:/" +
                    env.getProjectName() + "-" + StringUtils.Replace(env.getProjectVersion(), ".", "-") + "/TEST", "/", "\\");
                this.getDao().getConfigurationInstance().utils.AddToSystemPath(true, path,
                    ($status : boolean) : void => {
                        this.assertEquals($status, true, "Validate that WIN Path has been added");
                        if ($status) {
                            this.getDao().getConfigurationInstance().utils.SystemPathExists(path,
                                ($status : boolean) : void => {
                                    this.assertEquals($status, true, "Validate existence of WIN Path");
                                    if ($status) {
                                        this.getDao().getConfigurationInstance().utils.RemoveSystemPath(path,
                                            ($status : boolean) : void => {
                                                this.assertEquals($status, true, "Validate that WIN Path has been removed");
                                                if ($status) {
                                                    this.getDao().getConfigurationInstance().utils.SystemPathExists(path,
                                                        ($status : boolean) : void => {
                                                            this.assertEquals($status, false, "Validate non-existence of WIN Path");
                                                        });
                                                }
                                            });
                                    }
                                });
                        }
                    });
            });
        }

        public testValidate() : void {
            this.addButton("Run validation", () : void => {
                this.runProtocol(InstallationProtocolType.VALIDATE);
            });
        }

        public testInstall() : void {
            this.addButton("Run Install chain", () : void => {
                this.runProtocol(InstallationProtocolType.INSTALL);
            });
            this.addButton("Stop installation", () : void => {
                this.installDao.Abort();
            });
        }

        public testUninstall() : void {
            this.addButton("Run Uninstall chain", () : void => {
                this.runProtocol(InstallationProtocolType.UNINSTALL);
            });
        }

        public testFix() : void {
            this.addButton("Run Repair chain", () : void => {
                this.runProtocol(InstallationProtocolType.REPAIR);
            });
        }

        protected before() : IRuntimeTestPromise {
            this.installDao = new InstallationRecipeDAO();
            this.installDao.setConfigurationPath(this.installScript);
            this.installDao.ConfigurationLibrary().Log = this.installDao.ConfigurationLibrary().Echo;

            let rootPath : string = this.getRequest().getHostUrl();
            if (StringUtils.Contains(rootPath, ".html")) {
                rootPath = StringUtils.Substring(rootPath, 0, StringUtils.IndexOf(rootPath, "/", false));
            }
            Echo.Println("<i id=\"LoadingScript\">Loading install script ...</i>");
            Echo.Println("<span id=\"ScriptProcess\"></span>");

            this.installDao.getEvents().setOnStart(() : void => {
                Echo.Printf("Running installation recipe version \"{0}\".", this.installDao.getConfigurationVersion());
                WindowManager.ScrollToBottom();
            });

            let estimation : string;
            let startTime : number;
            this.installDao.getEvents().setOnChange(($eventArgs : InstallationProgressEventArgs) : void => {
                if ($eventArgs.ProgressCode() === InstallationProgressCode.DOWNLOAD_START) {
                    startTime = new Date().getTime();
                }
                if ($eventArgs.ProgressCode() === InstallationProgressCode.DOWNLOAD_CHANGE) {
                    const forecast : number =
                        ($eventArgs.RangeEnd() - $eventArgs.CurrentValue()) /
                        ($eventArgs.CurrentValue() / ((new Date().getTime() - startTime) / 1000));
                    if (forecast < 60) {
                        estimation = Convert.ToFixed(forecast, 0) + " s";
                    } else if (forecast > 60 && forecast < 3600) {
                        estimation = Convert.ToFixed(forecast / 60, 0) + " min";
                    } else if (forecast > 3600) {
                        estimation = Convert.ToFixed(forecast / 3600, 0) + " h";
                    }
                    ElementManager.setInnerHtml("ScriptProcess", StringUtils.Format("<b>Get data source:</b> {0}, ({3}) {1}/{2}",
                        $eventArgs.Message(), $eventArgs.CurrentValue() + "", $eventArgs.RangeEnd() + "", estimation));
                    window.scrollTo(0, ElementManager.getElement("ScriptProcess").offsetTop);
                } else if ($eventArgs.ProgressCode() === InstallationProgressCode.STEP_START) {
                    Echo.Printf("Step {0}, {1}/{2}", $eventArgs.Message(), $eventArgs.CurrentValue() + "", $eventArgs.RangeEnd() + "");
                } else {
                    if ($eventArgs.CurrentValue() > -1) {
                        Echo.Printf("{0}, {1}/{2}", $eventArgs.Message(), $eventArgs.CurrentValue() + "", $eventArgs.RangeEnd() + "");
                    } else {
                        Echo.Println($eventArgs.Message());
                    }
                    WindowManager.ScrollToBottom();
                }
            });
            this.installDao.getEvents().setOnComplete(() : void => {
                Echo.Println("Installation has finished.");
                WindowManager.ScrollToBottom();
            });
            this.installDao.getEvents().setOnError(($eventArgs : ErrorEventArgs) : void => {
                Echo.Printf("ERROR: {0}", $eventArgs.Message());
                WindowManager.ScrollToBottom();
            });

            return ($done : () => void) : void => {
                this.installDao.Load(LanguageType.EN, () : void => {
                    ElementManager.Hide("LoadingScript");
                    let configPath : string = this.installDao.getDaoDataSource();
                    if (!StringUtils.Contains(configPath, "http://", "https://", "file://", "www.")) {
                        configPath = rootPath + "/" + configPath;
                    }
                    Echo.Println("<b>Configuration data source:</b> " + configPath + StringUtils.NewLine());
                    if (ObjectValidator.IsEmptyOrNull(this.installDao.getChain()) || this.forceChain) {
                        this.installDao.getStaticConfiguration().getChain = () : string[] => {
                            return this.chain;
                        };
                    }
                    $done();
                });
            };
        }

        protected setScript($path : string) : void {
            if (!ObjectValidator.IsEmptyOrNull($path)) {
                this.installScript = $path;
            }
        }

        protected setChain($value : string[], $force : boolean = false) : void {
            if (!ObjectValidator.IsEmptyOrNull($value)) {
                this.chain = $value;
                this.forceChain = $force;
            }
        }

        protected getDao() : InstallationRecipeDAO {
            return this.installDao;
        }

        protected runProtocol($type : InstallationProtocolType) : void {
            switch ($type) {
            case InstallationProtocolType.VALIDATE:
                Echo.Println("<h2>Validation report</h2>");
                break;
            case InstallationProtocolType.INSTALL:
                Echo.Println("<h2>Installation report</h2>");
                break;
            case InstallationProtocolType.UNINSTALL:
                Echo.Println("<h2>Uninstallation report</h2>");
                break;
            case InstallationProtocolType.REPAIR:
                Echo.Println("<h2>Repair report</h2>");
                break;
            default:
                Echo.Println("<h2>Report for: " + $type + "</h2>");
                break;
            }
            this.installDao.RunInstallationChain($type);
        }
    }
}
/* dev:end */
