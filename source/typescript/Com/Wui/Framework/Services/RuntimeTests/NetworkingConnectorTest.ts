/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Services.RuntimeTests {
    "use strict";
    import NetworkingConnector = Com.Wui.Framework.Services.Connectors.NetworkingConnector;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;

    export class NetworkingConnectorTest extends Com.Wui.Framework.Gui.HttpProcessor.Resolvers.RuntimeTestRunner {
        private networkingConnector : NetworkingConnector;

        public before() : void {
            this.networkingConnector = new NetworkingConnector();
            this.networkingConnector.getEvents().OnError(($args : ErrorEventArgs) : void => {
                Echo.Println($args.Message());
            });
        }

        public testSingleGetFreePort() : void {
            this.addButton("Find free port", () : void => {
                this.networkingConnector
                    .getFreePort()
                    .Then(($port : number) : void => {
                        Echo.Printf("<b>Free port found: </b>" + $port);
                    });
            });
        }

        public testRepeatGetFreePort() : void {
            this.addButton("repeat getFreePort request - async", () : void => {
                let lastPort : number = -1;

                const getNextPort : any = ($index : number) : void => {
                    if ($index < 20) {
                        this.networkingConnector
                            .getFreePort()
                            .Then(($port : number) : void => {
                                if (lastPort === $port) {
                                    Echo.Println("<b style=\"color: red;\">Ports are identical<b/>[" + $index + "]");
                                } else {
                                    Echo.Println("<span style=\"color: green;\">OK [" + $index + "]</span>");
                                }
                                lastPort = $port;
                                getNextPort($index + 1);
                            });
                    }
                };
                getNextPort(0);
            });

            this.addButton("repeat getFreePort request - sync", () : void => {
                let lastPort : number = -1;

                for (let $index : number = 0; $index < 20; $index++) {
                    this.networkingConnector
                        .getFreePort()
                        .Then(($port : number) : void => {
                            if (lastPort === $port) {
                                Echo.Println("<b style=\"color: red;\">Ports are identical<b/>[" + $index + "]");
                            } else {
                                Echo.Println("<span style=\"color: green;\">OK [" + $index + "]</span>");
                            }
                            lastPort = $port;
                        });
                }
            });
        }
    }
}
/* dev:end */
