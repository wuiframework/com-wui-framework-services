/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Services.RuntimeTests {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;
    import CertsConnector = Com.Wui.Framework.Services.Connectors.CertsConnector;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IRuntimeTestPromise = Com.Wui.Framework.Commons.HttpProcessor.Resolvers.IRuntimeTestPromise;

    export class CertsConnectorTest extends Com.Wui.Framework.Gui.HttpProcessor.Resolvers.RuntimeTestRunner {
        private connector : CertsConnector;

        public before() : void {
            this.connector = new CertsConnector();
            this.connector.getEvents().OnError(($args : ErrorEventArgs) : void => {
                Echo.Println($args.Message());
            });
        }

        public testGetCertsForLocalhost() : IRuntimeTestPromise {
            return ($done : () => void) : void => {
                this.connector
                    .getCertsFor("localhost.wuiframework.com")
                    .Then(($cert : string, $key : string) : void => {
                        this.assertEquals(ObjectValidator.IsEmptyOrNull($cert), false);
                        this.assertEquals(ObjectValidator.IsEmptyOrNull($key), false);
                        $done();
                    });
            };
        }

        public testGetCertsFromBlackList() : IRuntimeTestPromise {
            return ($done : () => void) : void => {
                this.connector
                    .getCertsFor("www.wuiframework.com")
                    .Then(($cert : string, $key : string) : void => {
                        this.assertEquals(ObjectValidator.IsEmptyOrNull($cert), true);
                        this.assertEquals(ObjectValidator.IsEmptyOrNull($key), true);
                        $done();
                    });
            };
        }
    }
}
/* dev:end */
