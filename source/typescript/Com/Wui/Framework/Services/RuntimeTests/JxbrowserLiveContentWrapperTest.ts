/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Services.RuntimeTests {
    "use strict";
    import LiveContentWrapper = Com.Wui.Framework.Services.WebServiceApi.LiveContentWrapper;
    import WebServiceClientFactory = Com.Wui.Framework.Services.WebServiceApi.WebServiceClientFactory;
    import WebServiceClientType = Com.Wui.Framework.Commons.Enums.WebServiceClientType;
    import IWebServiceClient = Com.Wui.Framework.Services.Interfaces.IWebServiceClient;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;

    export class JxbrowserLiveContentWrapperTest extends Com.Wui.Framework.Gui.HttpProcessor.Resolvers.RuntimeTestRunner {

        private client : IWebServiceClient;
        private serverClassName : string = "Com.Wui.Framework.Jcommons.TestClass";

        public setUp() : void {
            this.client = WebServiceClientFactory.getClient(WebServiceClientType.JXBROWSER_BRIDGE);
            this.client.setResponseFormatter(null);
        }

        public testMethodCalls() : void {
            this.addButton("string TestMethod(double, float)", () : void => {
                LiveContentWrapper.InvokeMethod(this.client, this.serverClassName, "TestMethod",
                    1500.16498494, 2500.51561684986).Then(($response : string) => {
                    Echo.Println("Returned : " + $response);
                });
            });

            this.addButton("float TestMethod(string, float)", () : void => {
                LiveContentWrapper.InvokeMethod(this.client, this.serverClassName, "TestMethod",
                    "Test String", 45645.5156635434541684986).Then(($response : number) => {
                    Echo.Println("Returned : " + $response);
                });
            });

            this.addButton("Non-existent method", () : void => {
                LiveContentWrapper.InvokeMethod(this.client, this.serverClassName, "TestMethodd",
                    "Test String", 45645.5156635434541684986).Then(($response : number) => {
                    Echo.Println("Returned : " + $response);
                });
            });
        }
    }
}
/* dev:end */
