/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Services.RuntimeTests {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import AuthHttpResolver = Com.Wui.Framework.Services.ErrorPages.AuthHttpResolver;
    import LiveContentWrapper = Com.Wui.Framework.Services.WebServiceApi.LiveContentWrapper;
    import IWebServiceClient = Com.Wui.Framework.Services.Interfaces.IWebServiceClient;
    import WebServiceClientFactory = Com.Wui.Framework.Services.WebServiceApi.WebServiceClientFactory;
    import WebServiceClientType = Com.Wui.Framework.Services.Enums.WebServiceClientType;
    import UserManagerConnector = Com.Wui.Framework.Services.Connectors.UserManagerConnector;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import StaticPageContentManager = Com.Wui.Framework.Gui.Utils.StaticPageContentManager;
    import ElementManager = Com.Wui.Framework.Gui.Utils.ElementManager;
    import ElementEventsManager = Com.Wui.Framework.Gui.Events.ElementEventsManager;

    class LogInPage extends AuthHttpResolver {
        private service : UserManagerConnector;

        public constructor() {
            super();
            this.service = new UserManagerConnector(false,
                "https://localhost.wuiframework.com/connector.config.jsonp");
            this.setExpirationTime("10 sec");
        }

        protected resolver() : void {
            const loginFormId : string = "LogInForm";
            const registerFormId : string = "RegisterForm";
            if (!ElementManager.Exists(loginFormId)) {
                const loginNameId : string = "LoginUserNameId";
                const loginPassId : string = "LoginPasswordId";
                const loginButtonId : string = "LoginLogInButtonId";
                const registerNameId : string = "RegisterUserNameId";
                const registerPassId : string = "RegisterPasswordId";
                const registerButtonId : string = "RegisterLogInButtonId";

                StaticPageContentManager.BodyAppend(
                    "<div id=\"" + loginFormId + "\">" +
                    "<h2>Log in</h2>" +
                    "Username<br><input type=\"text\" id=\"" + loginNameId + "\"><br>" +
                    "Password<br><input type=\"password\" id=\"" + loginPassId + "\"><br>" +
                    "<div style=\"border: 1px solid red; cursor: pointer; " +
                    "width: 250px; overflow-x: hidden; overflow-y: hidden; " +
                    "text-align: center; font-size: 16px; font-family: Consolas; color: red;\" " +
                    "id=\"" + loginButtonId + "\">Log in</div>" +
                    "</div>");

                const eventsLogin : ElementEventsManager = new ElementEventsManager(loginButtonId);
                eventsLogin.setOnClick(() : void => {
                    this.service
                        .LogIn(
                            (<HTMLInputElement>ElementManager.getElement(loginNameId)).value,
                            (<HTMLInputElement>ElementManager.getElement(loginPassId)).value)
                        .Then(($token : string) : void => {
                            if (!ObjectValidator.IsEmptyOrNull($token)) {
                                ElementManager.Hide(loginFormId);
                                ElementManager.Hide(registerFormId);
                                Echo.Printf("You have successfully logged in with token: {0}", $token);
                                this.reinvokeRequest($token);
                            } else {
                                Echo.Printf("Invalid user, please try again.");
                            }
                        });
                });

                StaticPageContentManager.BodyAppend(
                    "<div id=\"" + registerFormId + "\">" +
                    "<h2>Register</h2>" +
                    "Username<br><input type=\"text\" id=\"" + registerNameId + "\"><br>" +
                    "Password<br><input type=\"password\" id=\"" + registerPassId + "\"><br>" +
                    "<div style=\"border: 1px solid red; cursor: pointer; " +
                    "width: 250px; overflow-x: hidden; overflow-y: hidden; " +
                    "text-align: center; font-size: 16px; font-family: Consolas; color: red;\" " +
                    "id=\"" + registerButtonId + "\">Register</div>" +
                    "</div>");

                const eventsRegister : ElementEventsManager = new ElementEventsManager(registerButtonId);
                eventsRegister.setOnClick(() : void => {
                    this.service
                        .Register(
                            (<HTMLInputElement>ElementManager.getElement(registerNameId)).value,
                            (<HTMLInputElement>ElementManager.getElement(registerPassId)).value)
                        .Then(($status : boolean) : void => {
                            if ($status) {
                                Echo.Printf("You have successfully registered.");
                                ElementManager.Hide(registerFormId);
                            } else {
                                Echo.Printf("Registration has failed.");
                            }
                        });
                });
                this.getEventsManager().FireAsynchronousMethod(() : void => {
                    eventsLogin.Subscribe();
                    eventsRegister.Subscribe();
                });
            } else {
                ElementManager.Show(loginFormId);
                ElementManager.Show(registerFormId);
            }
        }
    }

    export class LogInTest extends Com.Wui.Framework.Gui.HttpProcessor.Resolvers.RuntimeTestRunner {
        public testInvokeSecuredFunction() : void {
            const loginFormId : string = "LogInForm";
            const registerFormId : string = "RegisterForm";

            const client : IWebServiceClient = WebServiceClientFactory.getClient(WebServiceClientType.WUI_HUB_CONNECTOR,
                "https://localhost.wuiframework.com/connector.config.jsonp");

            this.addButton("Invoke secure function", () : void => {
                LiveContentWrapper.InvokeMethod(client,
                    "Com.Wui.Framework.Hub.RuntimeTests.UserManagerTest", "SecureCall")
                    .Then(($success : boolean) : void => {
                        ElementManager.Hide(loginFormId);
                        ElementManager.Hide(registerFormId);
                        if ($success) {
                            Echo.Printf("Secured function successfully invoked.");
                        } else {
                            Echo.Printf("Secured function invocation failed.");
                        }
                    });
            });

            this.addButton("Invoke admin function", () : void => {
                LiveContentWrapper.InvokeMethod(client,
                    "Com.Wui.Framework.Hub.RuntimeTests.UserManagerTest", "SecureAdminCall")
                    .Then(($success : boolean) : void => {
                        ElementManager.Hide(loginFormId);
                        ElementManager.Hide(registerFormId);
                        if ($success) {
                            Echo.Printf("Secured function successfully invoked.");
                        } else {
                            Echo.Printf("Secured function invocation failed.");
                        }
                    });
            });
        }

        public testInvalidateToken() : void {
            this.addButton("Invalidate token", () : void => {
                AuthHttpResolver.LogOut();
                Echo.Printf("User has been logged out.");
            });
        }

        public __IgnoretestLoginFacebook() : void {
            // example url for logging into fb/getting token by redirecting
            //
            // https://www.facebook.com/dialog/oauth/
            // ?client_id=201158640315306&redirect_uri=http://localhost.wuiframework.com/com-wui-framework-services/
        }

        public __IgnoretestLoginGoogle() : void {
            // example url for logging into google/getting token by redirecting
            //
            // https://accounts.google.com/o/oauth2/
            // v2/auth?scope=email%20profile&redirect_uri=http://localhost.wuiframework.com/com-wui-framework-services
            // &response_type=code&client_id=176530116802-vfhiqjrgt5f5th6tokqutgp3ke3dpafn.apps.googleusercontent.com
        }

        protected before() : void {
            this.overrideResolver("/ServerError/Http/NotAuthorized", LogInPage);
        }
    }
}
/* dev:end */
