/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Services.WebServiceApi.Clients {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ObjectEncoder = Com.Wui.Framework.Commons.Utils.ObjectEncoder;
    import WebSocketsClient = Com.Wui.Framework.Commons.WebServiceApi.Clients.WebSocketsClient;
    import ObjectDecoder = Com.Wui.Framework.Commons.Utils.ObjectDecoder;
    import EventType = Com.Wui.Framework.Commons.Enums.Events.EventType;
    import WebServiceConfiguration = Com.Wui.Framework.Commons.WebServiceApi.WebServiceConfiguration;
    import Property = Com.Wui.Framework.Commons.Utils.Property;

    /**
     * ForwardingClient class provides client for web sockets communication forwarded over WUI Hub agents.
     */
    export class ForwardingClient extends WebSocketsClient {
        private agentName : string;

        constructor($configuration : WebServiceConfiguration) {
            super($configuration);
            this.agentName = "WuiConnector";
        }

        /**
         * @param {string} $value Specify agent name used for look up on WUI Hub.
         * @return {string} Returns agent name value.
         */
        public AgentName($value : string) : string {
            return this.agentName = Property.String(this.agentName, $value);
        }

        protected sendRequest($data : string) : void {
            const protocol : any = JSON.parse($data);
            protocol.data = ObjectEncoder.Base64(JSON.stringify({
                args: ObjectEncoder.Base64(JSON.stringify([this.agentName, protocol])),
                name: "Com.Wui.Framework.Hub.Utils.WuiConnectorHub.ForwardRequest"
            }));
            $data = JSON.stringify(protocol);
            super.sendRequest($data);
        }

        protected onResponse($event : MessageEvent) : void {
            try {
                if ($event.data !== "OK" && !ObjectValidator.IsEmptyOrNull($event.data)) {
                    let data : any = JSON.parse($event.data);
                    data.data.returnValue = ObjectDecoder.Base64(data.data.returnValue);
                    if (ObjectValidator.IsBoolean(data.data.returnValue) && !data.data.returnValue || data.data.returnValue === "false") {
                        this.throwError("Failed to forward request, no agent found.");
                    } else {
                        data = JSON.parse(data.data.returnValue);
                        if (data.type === EventType.ON_CHANGE) {
                            data.data.origin = this.getServerUrl();
                            super.onResponse(<any>{
                                data: data.data
                            });
                        }
                    }
                } else {
                    super.onResponse($event);
                }
            } catch (ex) {
                this.throwError(ex);
            }
        }
    }
}
