/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Services.WebServiceApi {
    "use strict";
    import PersistenceFactory = Com.Wui.Framework.Commons.PersistenceApi.PersistenceFactory;
    import WebServiceConfiguration = Com.Wui.Framework.Commons.WebServiceApi.WebServiceConfiguration;
    import IWebServiceRequestFormatterData = Com.Wui.Framework.Commons.Interfaces.IWebServiceRequestFormatterData;
    import Exception = Com.Wui.Framework.Commons.Exceptions.Type.Exception;
    import IWebServiceException = Com.Wui.Framework.Services.Interfaces.IWebServiceException;
    import WebServiceClientEventType = Com.Wui.Framework.Commons.Enums.Events.WebServiceClientEventType;
    import WebServiceClientType = Com.Wui.Framework.Services.Enums.WebServiceClientType;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import IWebServiceProtocol = Com.Wui.Framework.Services.Interfaces.IWebServiceProtocol;
    import ObjectDecoder = Com.Wui.Framework.Commons.Utils.ObjectDecoder;
    import ObjectEncoder = Com.Wui.Framework.Commons.Utils.ObjectEncoder;
    import Parent = Com.Wui.Framework.Commons.WebServiceApi.WebServiceClientFactory;
    import Clients = Com.Wui.Framework.Commons.WebServiceApi.Clients;
    import WindowManager = Com.Wui.Framework.Gui.Utils.WindowManager;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import IWebServiceClient = Com.Wui.Framework.Services.Interfaces.IWebServiceClient;
    import IPersistenceHandler = Com.Wui.Framework.Commons.Interfaces.IPersistenceHandler;
    import WuiBuilderConnector = Com.Wui.Framework.Commons.Connectors.WuiBuilderConnector;
    import HttpStatusType = Com.Wui.Framework.Commons.Enums.HttpStatusType;
    import HttpRequestParser = Com.Wui.Framework.Commons.HttpProcessor.HttpRequestParser;
    import ForwardingClient = Com.Wui.Framework.Services.WebServiceApi.Clients.ForwardingClient;

    export class WebServiceClientFactory extends Com.Wui.Framework.Commons.WebServiceApi.WebServiceClientFactory {

        /**
         * @param {WebServiceClientType} $clientType Specify client type, which should be validated.
         * @return {boolean} Returns true, if required client type is supported by runtime environment, othewise false.
         */
        public static IsSupported($clientType : WebServiceClientType) : boolean {
            if ($clientType === WebServiceClientType.WUI_REST_CONNECTOR) {
                return Parent.IsSupported(WebServiceClientType.AJAX) ||
                    Parent.IsSupported(WebServiceClientType.IFRAME) ||
                    Parent.IsSupported(WebServiceClientType.POST_MESSAGE);
            } else if (
                $clientType === WebServiceClientType.WUI_DESKTOP_CONNECTOR ||
                $clientType === WebServiceClientType.WUI_HUB_CONNECTOR ||
                $clientType === WebServiceClientType.WUI_FORWARD_CLIENT ||
                $clientType === WebServiceClientType.WUI_HOST_CLIENT ||
                $clientType === WebServiceClientType.WUI_BUILDER_CONNECTOR) {
                return Parent.IsSupported(WebServiceClientType.WEB_SOCKETS) ||
                    Parent.IsSupported(WebServiceClientType.HTTP);
            } else {
                return Parent.IsSupported($clientType);
            }
        }

        /**
         * @param {number} $clientId Specify id of the desired service client.
         * @return {IWebServiceClient} Returns instance of web service client based on $clientId,
         * if client has been found, otherwise null.
         */
        public static getClientById($clientId : number) : IWebServiceClient {
            return Parent.getClientById($clientId);
        }

        protected static createClientInstance($clientType : WebServiceClientType,
                                              $serverConfigurationSource? : string | WebServiceConfiguration) : IWebServiceClient {
            let configuration : WebServiceConfiguration;
            if (ObjectValidator.IsString($serverConfigurationSource) ||
                ObjectValidator.IsEmptyOrNull($serverConfigurationSource)) {
                configuration = new WebServiceConfiguration(<string>$serverConfigurationSource);
            } else {
                configuration = <WebServiceConfiguration>$serverConfigurationSource;
            }
            let client : IWebServiceClient = null;
            const request : HttpRequestParser = Loader.getInstance().getHttpManager().getRequest();

            if (WebServiceClientFactory.IsSupported($clientType)) {
                switch ($clientType) {
                case WebServiceClientType.WUI_BUILDER_CONNECTOR:
                    client = WebServiceClientFactory.getClientById(WuiBuilderConnector.Connect().getId());
                    break;
                case WebServiceClientType.WUI_HOST_CLIENT:
                    configuration = new WebServiceConfiguration(request.getHostUrl() + "/connector.config.jsonp");
                    if (WebServiceClientFactory.IsSupported(WebServiceClientType.WEB_SOCKETS)) {
                        if (StringUtils.StartsWith(request.getHostUrl(), "https://")) {
                            configuration.ServerProtocol("wss");
                        } else {
                            configuration.ServerProtocol("ws");
                        }
                        client = new Clients.WebSocketsClient(configuration);
                    } else {
                        if (StringUtils.StartsWith(request.getHostUrl(), "https://")) {
                            configuration.ServerProtocol("https");
                        } else {
                            configuration.ServerProtocol("http");
                        }
                        client = new Clients.AjaxClient(configuration);
                    }
                    break;
                case WebServiceClientType.WUI_REST_CONNECTOR:
                case WebServiceClientType.WUI_HUB_CONNECTOR:
                case WebServiceClientType.WUI_FORWARD_CLIENT:
                case WebServiceClientType.WUI_DESKTOP_CONNECTOR:
                    if (ObjectValidator.IsEmptyOrNull($serverConfigurationSource)) {
                        if ($clientType === WebServiceClientType.WUI_REST_CONNECTOR ||
                            $clientType === WebServiceClientType.WUI_HUB_CONNECTOR ||
                            $clientType === WebServiceClientType.WUI_FORWARD_CLIENT) {
                            if (request.IsOnServer(true)) {
                                $serverConfigurationSource =
                                    "https://localhost.wuiframework.com/connector.config.jsonp";
                            } else {
                                $serverConfigurationSource = "https://hub.wuiframework.com/connector.config.jsonp";
                            }
                            configuration = new WebServiceConfiguration($serverConfigurationSource);
                        } else {
                            $serverConfigurationSource = "resource/libs/WuiConnector/connector.config.jsonp";
                        }
                    }
                    if (ObjectValidator.IsString($serverConfigurationSource)) {
                        configuration = new WebServiceConfiguration(<string>$serverConfigurationSource);
                        configuration.ServerAddress("127.0.0.1");
                        configuration.ServerPort(8888);
                    } else {
                        configuration = <WebServiceConfiguration>$serverConfigurationSource;
                    }

                    if ($clientType !== WebServiceClientType.WUI_REST_CONNECTOR &&
                        WebServiceClientFactory.IsSupported(WebServiceClientType.WEB_SOCKETS)) {
                        if (StringUtils.StartsWith(request.getHostUrl(), "https://")) {
                            configuration.ServerProtocol("wss");
                        } else {
                            configuration.ServerProtocol("ws");
                        }
                        if ($clientType === WebServiceClientType.WUI_FORWARD_CLIENT) {
                            client = new ForwardingClient(configuration);
                        } else {
                            client = new Clients.WebSocketsClient(configuration);
                        }
                    } else {
                        if (StringUtils.StartsWith(request.getHostUrl(), "https://")) {
                            configuration.ServerProtocol("https");
                        } else {
                            configuration.ServerProtocol("http");
                        }
                        if (request.IsSameOrigin(configuration.getServerUrl())) {
                            client = new Clients.AjaxClient(configuration);
                        } else if (WebServiceClientFactory.IsSupported(WebServiceClientType.POST_MESSAGE)) {
                            client = new Clients.PostMessageClient(configuration);
                        } else {
                            configuration.ResponseUrl(configuration.getServerUrl() + "connector.response.jsonp");
                            client = new Clients.IframeClient(configuration);
                        }
                    }
                    break;
                default :
                    client = Parent.createClientInstance($clientType, $serverConfigurationSource);
                }
            }

            if (!ObjectValidator.IsEmptyOrNull(client)) {
                if (!client.IsTypeOf(Clients.WebSocketsClient)) {
                    client.getEvents().OnStart(() : void => {
                        const closeUrl : string =
                            client.getServerUrl() + "CloseClient?id=" + ObjectEncoder.Base64(client.getId().toString());
                        const closeClient : any = ($url : string) : void => {
                            if (!StringUtils.ContainsIgnoreCase($url, "ws://", "wss://")) {
                                const closeRequest : HTMLScriptElement = <HTMLScriptElement>document.createElement("script");
                                closeRequest.src = $url + "&dummy=" + (new Date().getTime());
                                closeRequest.type = "text/javascript";
                                closeRequest.onload = () : void => {
                                    if (!ObjectValidator.IsEmptyOrNull(closeRequest.parentNode)) {
                                        closeRequest.parentNode.removeChild(closeRequest);
                                    }
                                };
                                document.body.appendChild(closeRequest);
                            }
                        };

                        const variableName : string = "pendingClose";
                        const persistence : IPersistenceHandler = PersistenceFactory.getPersistence("WebServiceClients");
                        if (persistence.Exists(variableName)) {
                            const pendingClose : string[] = persistence.Variable(variableName);
                            let index : number;
                            for (index = 0; index < pendingClose.length; index++) {
                                closeClient(pendingClose[index]);
                            }
                            persistence.Destroy(variableName);
                        }
                        WindowManager.getEvents().setBeforeRefresh(() : void => {
                            if (!ObjectValidator.IsEmptyOrNull(client) && client.CommunicationIsRunning()) {
                                let pendingClose : string[] = [];
                                if (persistence.Exists(variableName)) {
                                    pendingClose = persistence.Variable(variableName);
                                }
                                if (pendingClose.indexOf(closeUrl) === -1) {
                                    pendingClose.push(closeUrl);
                                }
                                persistence.Variable(variableName, pendingClose);
                            }
                        });
                        client.getEvents().OnClose(() : void => {
                            closeClient(closeUrl);
                        });
                    });
                }
                client.setRequestFormatter(($data : IWebServiceRequestFormatterData) : void => {
                    if (ObjectValidator.IsObject($data.value)) {
                        if (ObjectValidator.IsEmptyOrNull($data.value.id)) {
                            $data.value.id = StringUtils.getCrc(client.getUID());
                        }
                        // $data.value.origin = request.getBaseUrl();
                        // TODO: fix origin validation on server side
                        $data.value.origin = "http://www.wuiframework.com";
                        $data.value.status = HttpStatusType.CONTINUE;
                        $data.key = $data.value.id;
                    }
                });
                client.setResponseFormatter(($data : any, $owner : IWebServiceClient,
                                             $onSuccess : ($value : any, $key? : number) => void,
                                             $onError : ($message : string | Error | Exception) => void) : void => {
                    if (ObjectValidator.IsString($data)) {
                        $onSuccess($data);
                    } else {
                        const protocolData : IWebServiceProtocol = <IWebServiceProtocol>$data;
                        let serverOrigin : string = $owner.getServerUrl();
                        if (StringUtils.Contains(serverOrigin, ":") && StringUtils.EndsWith(serverOrigin, "/")) {
                            serverOrigin = StringUtils.Substring(serverOrigin, 0, StringUtils.Length(serverOrigin) - 1);
                        }
                        if (!ObjectValidator.IsSet(protocolData.origin) || (
                            !StringUtils.Contains(protocolData.origin, "127.0.0.1", "http://localhost", serverOrigin))) {
                            LogIt.Warning("Bad server origin: \"" + protocolData.origin + "\" from \"" + serverOrigin + "\"");
                        }
                        if (protocolData.status === HttpStatusType.SUCCESS) {
                            $onSuccess(protocolData, protocolData.id);
                        } else if (protocolData.status === HttpStatusType.ERROR) {
                            if (protocolData.type === "Server.Exception") {
                                const exceptionData : IWebServiceException = <IWebServiceException>protocolData.data;
                                const exception : Exception = new Exception(ObjectDecoder.Base64(exceptionData.message));
                                exception.Line(exceptionData.line);
                                exception.File(ObjectDecoder.Base64(exceptionData.file));
                                exception.Code(exceptionData.code);
                                exception.Stack(ObjectDecoder.Base64(exceptionData.stack));
                                $onError(exception);
                            } else if (protocolData.type === "Server.Timeout") {
                                Loader.getInstance().getHttpResolver().getEvents()
                                    .FireEvent("" + $owner.getId(), WebServiceClientEventType.ON_TIMEOUT);
                            } else if (protocolData.type === "Request.Exception") {
                                protocolData.type = WebServiceClientEventType.ON_ERROR;
                                $onSuccess(protocolData, protocolData.id);
                            } else {
                                $onError("Unsupported response error type: " + protocolData.type);
                            }
                        } else if (protocolData.status === HttpStatusType.NOT_AUTHORIZED) {
                            const postData : ArrayList<any> = new ArrayList<any>();
                            postData.Add(protocolData, "Protocol");
                            postData.Add($owner, "Client");
                            Loader.getInstance().getHttpManager().ReloadTo("/ServerError/Http/NotAuthorized", postData, true);
                        } else if (protocolData.status === HttpStatusType.NOT_FOUND) {
                            LogIt.Warning("Ignored unexpected response status 404. Continuing ...");
                        } else if (protocolData.status !== HttpStatusType.CONTINUE) {
                            $onError("Unsupported response status: " + protocolData.status);
                        }
                    }
                });
            }
            return client;
        }
    }
}
