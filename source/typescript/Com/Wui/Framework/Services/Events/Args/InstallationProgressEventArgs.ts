/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Services.Events.Args {
    "use strict";
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import InstallationProgressCode = Com.Wui.Framework.Services.Enums.InstallationProgressCode;

    /**
     * InstallationProgressEventArgs class provides args connected with installation progress.
     */
    export class InstallationProgressEventArgs extends Com.Wui.Framework.Commons.Events.Args.ProgressEventArgs {
        private message : string;
        private status : boolean;
        private code : InstallationProgressCode;

        /**
         * @param {string} [$message] Specify message, which describes current progress.
         */
        constructor($message? : string) {
            super();

            this.status = false;
            this.code = InstallationProgressCode.GENERAL;
            this.Message($message);
        }

        /**
         * @param {string} [$value] Set message, which describes current progress.
         * @return {string} Returns message value, which describes current progress.
         */
        public Message($value? : string) : string {
            return this.message = Property.String(this.message, $value);
        }

        /**
         * @param {boolean} [$value] Set actual status for current progress.
         * @return {boolean} Returns status for current progress.
         */
        public Status($value? : boolean) : boolean {
            return this.status = Property.Boolean(this.status, $value);
        }

        /**
         * @param {InstallationProgressCode} [$value] Set progress code.
         * @return {InstallationProgressCode} Returns progress code value.
         */
        public ProgressCode($value? : InstallationProgressCode) : InstallationProgressCode {
            if (ObjectValidator.IsSet($value)) {
                this.code = $value;
            }
            return this.code;
        }
    }
}
