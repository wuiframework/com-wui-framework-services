/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Services {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import StaticPageContentManger = Com.Wui.Framework.Gui.Utils.StaticPageContentManager;

    /**
     * Index request resolver class provides handling of web index page.
     */
    export class Index extends Com.Wui.Framework.Commons.HttpProcessor.Resolvers.BaseHttpResolver {

        protected resolver() : void {
            let output : string =
                "<div class=\"GuiInterface\">" +
                "<h1>WUI Framework Services Library</h1>" +
                "<h3>WUI Framework library focused on services and business logic for applications build on WUI Framework.</h3>" +
                "<div class=\"Index\">";

            /* dev:start */
            output +=
                "<H3>Runtime tests</H3>" +
                "<a href=\"" + RuntimeTests.RestWebServiceTest.CallbackLink() + "\">REST web services test</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTests.WebSocketsServiceTest.CallbackLink() + "\">WebSockets services test</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTests.ReportServiceTest.CallbackLink() + "\">Report service test</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTests.FileSystemConnectorTest.CallbackLink() + "\">FileSystem connector test</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTests.COMproxyConnectorTest.CallbackLink() + "\">COM Proxy test</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTests.FFIProxyConnectorTest.CallbackLink() + "\">FFI Proxy test</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTests.TerminalConnectorTest.CallbackLink() + "\">Terminal connector test</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTests.AgentsRegisterConnectorTest.CallbackLink() + "\">AgentsRegister connector test</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTests.NetworkingConnectorTest.CallbackLink() + "\">Networking connector test</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTests.LogInTest.CallbackLink() + "\">LogIn test</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTests.InstallationProcessTest.CallbackLink() + "\">Installation process test</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTests.WindowHandlerConnectorTest.CallbackLink() + "\">WindowHandler connector test</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTests.JxbrowserLiveContentWrapperTest.CallbackLink() + "\">Jxbrowser LiveContentWrapper Test</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTests.IdeHandlerConnectorTest.CallbackLink() + "\">Jxbrowser IdeHandlerConnector Test</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTests.VirtualBoxConnectorTest.CallbackLink() + "\">VirtualBox connector Test</a>" +
                StringUtils.NewLine() +
                "<a href=\"?debug=JRESimulator#/" + RuntimeTests.PageControllerTest.ClassNameWithoutNamespace() + "\">" +
                "PageController Test</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTests.RenderTest.CallbackLink() + "\">Render Test</a>" +
                StringUtils.NewLine() +
                "<a href=\"" + RuntimeTests.CertsConnectorTest.CallbackLink() + "\">Certs connector Test</a>" +
                StringUtils.NewLine();

            /* dev:end */
            output +=
                "</div>" +
                "</div>";

            output +=
                "<div class=\"Note\">" +
                "version: " + this.getEnvironmentArgs().getProjectVersion() +
                ", build: " + this.getEnvironmentArgs().getBuildTime() +
                "</div>" + StringUtils.NewLine(false) +
                "<div class=\"Logo\">" + StringUtils.NewLine(false) +
                "   <div class=\"WUI\"></div>" + StringUtils.NewLine(false) +
                "</div>";

            StaticPageContentManger.Clear();
            StaticPageContentManger.Title("WUI - Services Index");
            StaticPageContentManger.BodyAppend(output);
            StaticPageContentManger.Draw();
        }
    }
}
