/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Services.Enums {
    "use strict";

    export class WebServiceClientType extends Com.Wui.Framework.Commons.Enums.WebServiceClientType {
        public static readonly WUI_BUILDER_CONNECTOR : string = "WuiBuilderConnector";
        public static readonly WUI_DESKTOP_CONNECTOR : string = "WuiDesktopConnector";
        public static readonly WUI_REST_CONNECTOR : string = "WuiRestConnector";
        public static readonly WUI_HUB_CONNECTOR : string = "WuiHubConnector";
        public static readonly WUI_FORWARD_CLIENT : string = "WuiForwardClient";
        public static readonly WUI_HOST_CLIENT : string = "WuiHostClient";
    }
}
