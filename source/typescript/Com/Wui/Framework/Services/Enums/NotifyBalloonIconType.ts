/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Services.Enums {
    "use strict";

    export class NotifyBalloonIconType extends Com.Wui.Framework.Commons.Enums.WebServiceClientType {
        public static readonly NONE : string = "none";
        public static readonly INFO : string = "info";
        public static readonly WARNING : string = "warning";
        public static readonly ERROR : string = "error";
        public static readonly USER : string = "user";
    }
}
