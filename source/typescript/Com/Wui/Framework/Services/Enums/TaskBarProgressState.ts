/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Services.Enums {
    "use strict";

    export class TaskBarProgressState extends Com.Wui.Framework.Commons.Enums.WebServiceClientType {
        public static readonly NOPROGRESS : string = "NOPROGRESS";
        public static readonly INDETERMINATE : string = "INDETERMINATE";
        public static readonly NORMAL : string = "NORMAL";
        public static readonly ERROR : string = "ERROR";
        public static readonly PAUSED : string = "PAUSED";
    }
}
