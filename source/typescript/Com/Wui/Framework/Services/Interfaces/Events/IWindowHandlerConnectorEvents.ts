/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Services.Interfaces.Events {
    "use strict";

    export interface IWindowHandlerConnectorEvents extends Com.Wui.Framework.Commons.Interfaces.Events.IWebServiceClientEvents {
        OnNotifyIconClick($handler : () => void) : void;

        OnNotifyIconDoubleClick($handler : () => void) : void;

        OnNotifyIconBalloonClick($handler : () => void) : void;

        OnNotifyIconContextItemSelected($handler : ($name : string) => void) : void;

        OnWindowChanged($handler : (changedArgs : IWindowChangedEvent) => void) : void;
    }
}
