/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Services.Interfaces {
    "use strict";
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;

    export interface ILiveContentErrorPromise {

        OnError($callback : ($error : ErrorEventArgs, ...$args : any[]) => void) : ILiveContentPromise;

        Then($callback : (...$args : any[]) => void) : void;
    }

    export interface ILiveContentPromise {
        Then($callback : (...$args : any[]) => void) : void;
    }
}
