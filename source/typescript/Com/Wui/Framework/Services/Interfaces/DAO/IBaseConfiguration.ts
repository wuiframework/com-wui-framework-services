/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Services.Interfaces.DAO {
    "use strict";

    /**
     * Imports are focused on reuse of another configuration files
     */
    export abstract class IImports {
        /**
         * Specify variable name, which will be accessible in interfaces block
         */
        public variable : string;

        /**
         * Specify jsonp configuration source path from, which should be variable loaded
         */
        public source : string;
    }

    /**
     * Localization is suitable for extending of existing configuration to its language mutations
     */
    export abstract class ILocalization {
        /**
         * Specify jsonp configuration source path from, which should be loaded English localization
         */
        public en? : string;

        /**
         * Specify jsonp configuration source path from, which should be loaded Czech localization
         */
        public cz? : string;

        /**
         * Specify jsonp configuration source path from, which should be loaded Chinese localization
         */
        public cn? : string;
    }

    /**
     * IBaseConfiguration is minimal interface expected at any type of configuration
     */
    export abstract class IBaseConfiguration {
        /* tslint:disable: variable-name */
        /**
         * Specify version, which will be used as unique identifier for current version of the configuration file
         */
        public version? : string;

        /**
         * Specify jsonp configuration source path, which should be extended by the current configuration
         */
        public extendsConfig? : string;

        /**
         * Specify interface type, which should be used for identification of DAO parser
         * (currently available IBaseConfiguration, IBasePageConfiguration)
         */
        public $interface? : string;

        /**
         * Specify json object as array of variables,
         * which should be imported as instances of configuration files
         */
        public imports? : IImports[];

        /**
         * Specify language mutation settings for current configuration file
         */
        public localization? : ILocalization;

        /**
         * Get instance of parent configuration, if current configuration is extending another one
         */
        public $super : IBaseConfiguration;

        /**
         * @param {string} $messages Specify messages, which should be printed to Echo container in rendered GUI
         * Supported is string formatting by {0}, {1}, ...
         * @param {...any} $attributes Specify attributes for string formatting
         * @return {void}
         */
        public Echo : ($message : string, ...$attributes : any[]) => void;

        /**
         * @param {string} $messages Specify message, which should be printed as debug information.
         * Supported is string formatting by {0}, {1}, ...
         * @param {...any} $attributes Specify attributes for string formatting
         * @return {void}
         */
        public Log : ($message : string, ...$attributes : any[]) => void;

        /**
         * @param {string} $messages Specify message, which should be printed/notified as warning.
         * Supported is string formatting by {0}, {1}, ...
         * @param {...any} $attributes Specify attributes for string formatting
         * @return {void}
         */
        public Warning : ($message : string, ...$attributes : any[]) => void;

        /**
         * @param {string} $messages Specify message, which should be printed/notified as an error.
         * Supported is string formatting by {0}, {1}, ...
         * @param {...any} $attributes Specify attributes for string formatting
         * @return {void}
         */
        public Error : ($message : string, ...$attributes : any[]) => void;
        /* tslint:enable */
    }
}
