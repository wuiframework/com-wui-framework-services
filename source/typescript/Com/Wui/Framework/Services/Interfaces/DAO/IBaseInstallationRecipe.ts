/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Services.Interfaces.DAO {
    "use strict";
    import TerminalConnector = Com.Wui.Framework.Services.Connectors.TerminalConnector;
    import FileSystemHandlerConnector = Com.Wui.Framework.Services.Connectors.FileSystemHandlerConnector;
    import FileSystemDownloadOptions = Com.Wui.Framework.Services.Connectors.FileSystemDownloadOptions;
    import InstallationProgressCode = Com.Wui.Framework.Services.Enums.InstallationProgressCode;

    /**
     * IInstallationHandler is interface required by package functional API.
     */
    export interface IInstallationHandler {
        /* tslint:disable: no-duplicate-variable */
        (...$args : any[]) : void;

        ($status : boolean, $message? : string) : void;

        /* tslint:enable */
    }

    /**
     * IInstallationPackageSource is interface required by package source if source should be diverged based on platform type or
     * package source requires specification of additional HTTP request options.
     */
    export abstract class IInstallationPackageSource {
        /**
         * Specify global HTTP request options for diverged data sources.
         */
        public options? : FileSystemDownloadOptions;
        /**
         * Specify data source, which is specific for 32 bit platforms
         * as string or with additional HTTP request options.
         */
        public x86 : string | FileSystemDownloadOptions;
        /**
         * Specify data source, which is specific for 64 bit platforms
         * as string or with additional HTTP request options.
         */
        public x64 : string | FileSystemDownloadOptions;
    }

    /**
     * IInstallationPackage is interface, which provides detailed instruction for handling of Installation process.
     */
    export abstract class IInstallationPackage {
        /**
         * Readonly property, which reflex package name defined directly by its name in packages list.
         */
        public name? : string;
        /**
         * Package description, which should be used by installation process.
         */
        public description? : string;
        /**
         * Package info, which should be used by installation summary.
         */
        public info? : string;
        /**
         * Estimated package size after installation.
         */
        public size? : number;
        /**
         * Required package version, which should be validated.
         */
        public version? : string;
        /**
         * Package source, which will be used for package execution.
         */
        public source : string | FileSystemDownloadOptions | IInstallationPackageSource;
        /**
         * Condition, which should be used as decision maker for installation of the package.
         */
        public installCondition? : IInstallationHandler;
        /**
         * Function, which is able to select correct package source suitable for execution based
         * on current OS version and installation conditions. This function should be overridden only in case of that standard
         * implementation is not flexible enough.
         */
        public selectSource? : IInstallationHandler;
        /**
         * Function, which is able to serve and prepare package source for execution.
         * This function should be overridden only in case of that standard implementation is not flexible enough.
         */
        public getSource? : IInstallationHandler;
        /**
         * Function, which should be used for installation process.
         */
        public install? : IInstallationHandler;
        /**
         * Function, which should be used for uninstallation process.
         */
        public uninstall? : IInstallationHandler;
        /**
         * Function, which should be used for repair process.
         */
        public repair? : IInstallationHandler;
        /**
         * Function, which should be used for validation, if package is installed correctly.
         */
        public validate? : IInstallationHandler;
        /**
         * Specify, if package should be force processed.
         */
        public force? : boolean;
        /**
         * Specify, if package installation is optional.
         */
        public optional? : boolean;
    }

    export abstract class IInstallationRecipeNotifications {
        /* tslint:disable: variable-name */
        public package_is_installed_correctly : string;
        public package_is_missing_or_damaged__0__ : string;
        public package_is_already_installed : string;
        public step__0__has_been_skipped : string;
        public unsupported_callback_status__0__detected_for__1__at_step__2__ : string;
        public undefined_installation_protocol__0__ : string;
        public undefined_installation_chain__0__ : string;
        public unsupported_definition_of_package_source_for__0__ : string;
        public package_source__0__for__1__does_not_exist : string;
        public unable_to_clone_package_source_for__0__ : string;
        public required_GIT__0__has_not_been_found : string;
        public get_source__0__ : string;
        public unable_to_delete_downloaded_package : string;
        public unable_to_move_package_source_to_tmp_destination : string;
        public unable_to_create_installation_folder : string;
        public undefined_package_source_for__0__ : string;
        /* tslint:enable */
    }

    export abstract class IInstallationEnvironment extends IBaseConfiguration {
        /**
         * Automatically generated variable, which reflex currently processed package.
         */
        public block? : IInstallationPackage;
        /**
         * List of all available packages for installation chain in JSON format.
         */
        public packages : IInstallationPackage[];
        /**
         * List of all packages, which should be processed by current installation.
         */
        public getChain : () => string[];
        /**
         * Quick access to instance of Terminal connector, which can be required by installation functions.
         */
        public terminal? : TerminalConnector;
        /**
         * Quick access to instance of File System connector,
         * which can be required by installation functions.
         */
        public fileSystem? : FileSystemHandlerConnector;
        /**
         * Instance of Installation utils, which will be imported based on current platform.
         */
        public utils? : IInstallationUtils;
        /**
         * List of notifications used by installation logic.
         */
        public notifications : IInstallationRecipeNotifications;
        /**
         * @param {boolean} $status Specify on change status.
         * @param {string} $description Specify message, which describes current change.
         * @param {number} [$currentValue] Specify current change index.
         * @param {number} [$endValue] Specify expected changes count.
         * @param {InstallationProgressCode} [$code] Specify change code.
         * @return {void}
         */
        public onChange? : ($status : boolean, $description : string, $currentValue? : number, $endValue? : number,
                            $code? : InstallationProgressCode) => void;
    }
}
