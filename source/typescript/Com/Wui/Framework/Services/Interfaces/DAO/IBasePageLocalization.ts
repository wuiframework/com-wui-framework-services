/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Services.Interfaces.DAO {
    "use strict";

    /**
     * IBasePageLocalization is minimal interface expected at any type of configuration focused on Page localization
     */
    export abstract class IBasePageLocalization extends IBaseConfiguration {
        /**
         * Specify file path to the GUI static cache, which should be used for page fast load
         */
        public cacheFile? : string;

        /**
         * Specify page title value
         */
        public title? : string;

        /**
         * Specify text value for page loading label
         */
        public loading? : string;

        /**
         * Specify text value for page loading progress label
         */
        public loadingProgress? : string;
    }
}
