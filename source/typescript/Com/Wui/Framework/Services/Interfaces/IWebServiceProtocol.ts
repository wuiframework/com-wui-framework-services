/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Services.Interfaces {
    "use strict";

    export abstract class IWebServiceException {
        public message : string;
        public file : string;
        public line : number;
        public col : number;
        public code : number;
        public stack : string;
        public args? : any;
    }

    export abstract class ILiveContentProtocol {
        public name : string;
        public args : string;
        public returnValue : any;
    }

    export abstract class IWebServiceProtocol {
        public id? : number;
        public clientId? : number;
        public type? : string;
        public status? : number;
        public origin? : string;
        public token? : string;
        public data : string | IWebServiceException | ILiveContentProtocol | IWebServiceProtocol[];
    }
}
