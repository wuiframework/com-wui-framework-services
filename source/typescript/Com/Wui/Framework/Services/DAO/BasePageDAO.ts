/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Services.DAO {
    "use strict";
    import BasePanelViewerArgs = Com.Wui.Framework.Gui.Primitives.BasePanelViewerArgs;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IBasePageLocalization = Com.Wui.Framework.Services.Interfaces.DAO.IBasePageLocalization;

    /**
     * BasePageDAO class provides extension of BaseDAO focused on controllers for pages.
     */
    export class BasePageDAO extends BaseDAO {
        public static defaultConfigurationPath : string =
            "resource/data/Com/Wui/Framework/Services/Localization/BasePageLocalization.jsonp";

        protected modelArgs : BasePanelViewerArgs;

        /**
         * @return {IBasePageLocalization} Returns data, which are representing page configuration.
         */
        public getPageConfiguration() : IBasePageLocalization {
            return <IBasePageLocalization>super.getStaticConfiguration();
        }

        /**
         * @return {string} Returns file path, which should be used for page loading from its cache.
         */
        public getCacheFilePath() : string {
            return this.getPageConfiguration().cacheFile;
        }

        /**
         * @return {string} Returns value, which should be used as page title.
         */
        public getPageTitle() : string {
            return this.getPageConfiguration().title;
        }

        /**
         * @return {string} Returns value, which should be used as page loader text.
         */
        public getPageLoadingText() : string {
            return this.getPageConfiguration().loading;
        }

        /**
         * @return {string} Returns value, which should be used as page loader progress text.
         */
        public getPageLoadingProgressText() : string {
            return this.getPageConfiguration().loadingProgress;
        }

        /**
         * @return {BasePageViewerArgs} Returns arguments, which can be used as page viewer arguments.
         */
        public getModelArgs() : BasePanelViewerArgs {
            if (!ObjectValidator.IsSet(this.modelArgs)) {
                this.modelArgs = new BasePanelViewerArgs();
            }
            return this.modelArgs;
        }
    }
}
