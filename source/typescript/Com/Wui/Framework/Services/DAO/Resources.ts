/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Services.DAO {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import JsonUtils = Com.Wui.Framework.Commons.Utils.JsonUtils;

    export class Resources extends Com.Wui.Framework.Commons.IOApi.Handlers.JsonpFileReader {

        /**
         * @param {Object} $parent Specify data object, which should be extended.
         * @param {Object} $child Specify data object, which should be used for parent override and extend.
         * @return {Object} Returns extended data object.
         */
        public static Extend($parent : any, $child : any) : any {
            return JsonUtils.Extend($parent, $child);
        }

        /**
         * @param {Object} $source Specify data object, which should be deeply cloned.
         * @return {Object} Returns deep clone of the source data object.
         */
        public static DeepClone($source : any) : any {
            return JsonUtils.DeepClone($source);
        }
    }
}
