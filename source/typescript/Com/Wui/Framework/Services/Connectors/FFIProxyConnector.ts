/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    import WebServiceClientType = Com.Wui.Framework.Services.Enums.WebServiceClientType;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import LiveContentArgumentType = Com.Wui.Framework.Services.Enums.LiveContentArgumentType;
    import BaseConnector = Com.Wui.Framework.Services.Primitives.BaseConnector;

    /**
     * FFIProxyConnector class provides wrapping of FFI proxy services provided by WUI Connector instance.
     */
    export class FFIProxyConnector extends BaseConnector {

        /**
         * @param {string|FFIOptions} $fileOrOptions Specify name of the library file.
         * @return {IFFIProxyMethodPromise} Returns promise interface for handling of creation event.
         */
        public Load($fileOrOptions : string | FFIOptions) : IFFIProxyMethodPromise {
            return this.invoke("Load", $fileOrOptions);
        }

        /**
         * @param {string} $name Specify method name, which should be invoked.
         * @param {LiveContentArgumentType} $returnType Specify return type for method, which should be invoked.
         * @param {...any[]} [$args] Specify method arguments, which should be passed to the invoked method.
         * @return {IFFIProxyMethodPromise} Returns promise interface for handling of invoke method event.
         */
        public InvokeMethod($name : string, $returnType : LiveContentArgumentType, ...$args : any[]) : IFFIProxyMethodPromise {
            return this.invoke.apply(this, ["InvokeMethod", $name, $returnType].concat($args));
        }

        /**
         * @return {IFFIProxyMethodPromise} Returns promise interface for handling of release event.
         */
        public Release() : IFFIProxyMethodPromise {
            return this.invoke.apply(this, ["Release"]);
        }

        protected getServerNamespaces() : any {
            const namespaces : any = {};
            namespaces[WebServiceClientType.WUI_DESKTOP_CONNECTOR] = "Com.Wui.Framework.Connector.Connectors.FFIProxy";
            namespaces[WebServiceClientType.WUI_BUILDER_CONNECTOR] = "Com.Wui.Framework.Builder.Connectors.FFIProxy";
            namespaces[WebServiceClientType.WUI_FORWARD_CLIENT] = "Com.Wui.Framework.Connector.Connectors.FFIProxy";
            return namespaces;
        }
    }

    export interface IFFIProxyMethodPromise {
        Then($callback : ($returnValue : any, ...$args : any[]) => void) : void;
    }

    export abstract class FFIArgumentArray {
        public type : LiveContentArgumentType;
        public items : any[];
    }

    export abstract class FFIArgumentStruct {
        public typeName : string;
        public fields : any;
    }

    export abstract class FFIArgumentCallback {
        public typeName : string;
        public callback : any;
        public returnType? : LiveContentArgumentType;
        public args? : LiveContentArgumentType[] | FFIArgument[];
    }

    export abstract class FFIArgument {
        public type? : LiveContentArgumentType;
        public value : FFIArgumentArray | FFIArgumentStruct | FFIArgumentCallback | any;
    }

    export abstract class FFIOptions {
        public libraryPath : string;
        public env? : any;
        public cwd? : string;
    }
}
