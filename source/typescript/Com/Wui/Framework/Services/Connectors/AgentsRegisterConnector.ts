/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import BaseConnector = Com.Wui.Framework.Services.Primitives.BaseConnector;
    import LiveContentWrapper = Com.Wui.Framework.Services.WebServiceApi.LiveContentWrapper;
    import EventType = Com.Wui.Framework.Commons.Enums.Events.EventType;
    import WebServiceClientType = Com.Wui.Framework.Services.Enums.WebServiceClientType;
    import WebServiceConfiguration = Com.Wui.Framework.Commons.WebServiceApi.WebServiceConfiguration;
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;

    /**
     * AgentsRegisterConnector class provides wrapping of WUI Hub register of connected agents.
     */
    export class AgentsRegisterConnector extends BaseConnector {

        constructor($reconnect : boolean = true, $serverConfigurationSource? : string | WebServiceConfiguration) {
            super($reconnect, $serverConfigurationSource, WebServiceClientType.WUI_HUB_CONNECTOR);
        }

        public RegisterAgent($capabilities : IAgentCapabilities) : IAgentsRegisterPromise {
            const callbacks : any = {
                onComplete($success : boolean) : any {
                    // declare default callback
                },
                onError: null,
                onMessage($data : any) : any {
                    // declare default callback
                }
            };
            LiveContentWrapper.InvokeMethod(this.getClient(), this.getServerNamespace(),
                "RegisterAgent", $capabilities)
                .OnError(($error : ErrorEventArgs, ...$args : any[]) : void => {
                    this.errorHandler(callbacks.onError, $error, $args);
                })
                .Then(($result : any) : void => {
                    if (ObjectValidator.IsSet($result.type)) {
                        if ($result.type === EventType.ON_CHANGE) {
                            callbacks.onMessage($result.data);
                        }
                    } else if (ObjectValidator.IsBoolean($result)) {
                        callbacks.onComplete($result);
                    }
                });
            const promise : IAgentsRegisterPromise = {
                OnError  : ($callback : ($args : ErrorEventArgs) => void) : IAcknowledgePromise => {
                    callbacks.onError = $callback;
                    return promise;
                },
                OnMessage: ($callback : ($data : any) => void) : IAcknowledgePromise => {
                    callbacks.onMessage = $callback;
                    return promise;
                },
                Then     : ($callback : ($success : boolean) => void) : void => {
                    callbacks.onComplete = $callback;
                }
            };
            return promise;
        }

        public ForwardMessage($targetId : string, $data : any) : IAcknowledgePromise {
            return this.invoke("ForwardMessage", $targetId, $data);
        }

        public getAgentsList() : IAgentsListPromise {
            return this.invoke("getAgentsList");
        }

        public StartCommunication() : void {
            this.getClient().StartCommunication();
        }

        protected getServerNamespaces() : any {
            const namespaces : any = {};
            namespaces[WebServiceClientType.WUI_HUB_CONNECTOR] = "Com.Wui.Framework.Hub.Primitives.AgentsRegister";
            return namespaces;
        }

        protected init($serverConfigurationSource? : string | WebServiceConfiguration, $clientType? : WebServiceClientType) : void {
            if (ObjectValidator.IsEmptyOrNull($serverConfigurationSource)) {
                $serverConfigurationSource = "https://hub.wuiframework.com/connector.config.jsonp";
            }
            super.init($serverConfigurationSource, $clientType);
        }
    }

    export abstract class IAgentCapabilities {
        public name : string;
        public platform : string;
        public domain : string;
        public version : string;
    }

    export abstract class IAgentInfo extends IAgentCapabilities {
        public startTime : string;
    }

    export interface IAgentsRegisterPromise extends Com.Wui.Framework.Services.Connectors.IAcknowledgePromise {
        OnMessage($callback : ($data : any) => void) : IAcknowledgePromise;
    }

    export interface IAgentsListPromise {
        Then($callback : ($list : IAgentInfo[]) => void) : void;
    }
}
