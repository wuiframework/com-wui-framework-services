/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import LiveContentWrapper = Com.Wui.Framework.Services.WebServiceApi.LiveContentWrapper;
    import BaseConnector = Com.Wui.Framework.Services.Primitives.BaseConnector;
    import WebServiceClientType = Com.Wui.Framework.Services.Enums.WebServiceClientType;
    import IFileUploadProtocol = Com.Wui.Framework.Gui.Interfaces.Components.IFileUploadProtocol;

    export class UpdatesManagerConnector extends BaseConnector {

        public UploadConfiguration($appName : string, $configName : string, $version : string, $data : string) : IAcknowledgePromise {
            return LiveContentWrapper.InvokeMethod(this.getClient(), "Com.Wui.Framework.Hub.Utils.ConfigurationsManager",
                "Upload", $appName, $configName, $version, $data);
        }

        public UploadFile($data : IFileUploadProtocol) : IAcknowledgePromise {
            return LiveContentWrapper.InvokeMethod(this.getClient(), "Com.Wui.Framework.Hub.HttpProcessor.Resolvers.FileUploadResolver",
                "Upload", $data);
        }

        public UploadPackage($data : IFileUploadProtocol) : IAcknowledgePromise {
            return this.invoke("Upload", $data);
        }

        public RegisterPackage($appName : string, $releaseName : string, $platform : string, $version : string, $buildTime : string,
                               $fileName : string, $type? : string) : IAcknowledgePromise {
            return this.invoke("Register", $appName, $releaseName, $platform, $version, $buildTime, $fileName, $type);
        }

        /**
         * @param {string} $appName Specify application name, which should be validated.
         * @param {string} $releaseName Specify release name, which should be validated.
         * @param {string} $platform Specify platform type, which should be validated.
         * @param {string} $version Specify current application version.
         * @param {number} $buildTime Specify current application build time.
         * @param {boolean} [$fixedVersion=false] Specify if validation should be limited on specified version.
         * @return {IAcknowledgePromise} Returns promise interface for handling of update exists event.
         */
        public UpdateExists($appName : string, $releaseName : string, $platform : string, $version : string, $buildTime : number,
                            $fixedVersion : boolean = false) : IAcknowledgePromise {
            return this.invoke("UpdateExists", $appName, $releaseName, $platform, $version, $buildTime, $fixedVersion);
        }

        protected getClientType() : WebServiceClientType {
            return WebServiceClientType.WUI_HUB_CONNECTOR;
        }

        protected getServerNamespaces() : any {
            const namespaces : any = {};
            namespaces[WebServiceClientType.WUI_HUB_CONNECTOR] = "Com.Wui.Framework.Hub.Utils.SelfupdatesManager";
            return namespaces;
        }
    }
}
