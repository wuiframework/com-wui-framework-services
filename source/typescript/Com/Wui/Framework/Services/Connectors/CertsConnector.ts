/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    import WebServiceClientType = Com.Wui.Framework.Services.Enums.WebServiceClientType;
    import BaseConnector = Com.Wui.Framework.Services.Primitives.BaseConnector;

    /**
     * CertsConnector class provides wrapping of certificates handler provided by WUI Hub instance.
     */
    export class CertsConnector extends BaseConnector {

        /**
         * @param {string} $domain Specify domain name for which should be required certificate.
         * @return {ICertsConnectorPromise} Returns promise interface for handling of certificates.
         */
        public getCertsFor($domain : string) : ICertsConnectorPromise {
            return this.invoke("getCertsFor", $domain);
        }

        protected getClientType() : WebServiceClientType {
            return WebServiceClientType.WUI_HUB_CONNECTOR;
        }

        protected getServerNamespaces() : any {
            const namespaces : any = {};
            namespaces[WebServiceClientType.WUI_HUB_CONNECTOR] = "Com.Wui.Framework.Hub.Connectors.CertsHandler";
            return namespaces;
        }
    }

    export interface ICertsConnectorPromise {
        Then($callback : ($cert : string, $key : string) => void) : void;
    }
}
