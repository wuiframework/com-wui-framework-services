/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    import WindowStateType = Com.Wui.Framework.Services.Enums.WindowStateType;
    import WebServiceClientType = Com.Wui.Framework.Services.Enums.WebServiceClientType;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import IWebServiceClientEvents = Com.Wui.Framework.Commons.Interfaces.Events.IWebServiceClientEvents;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import WindowCornerType = Com.Wui.Framework.Gui.Enums.WindowCornerType;
    import EventType = Com.Wui.Framework.Commons.Enums.Events.EventType;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;
    import NotifyBalloonIconType = Com.Wui.Framework.Services.Enums.NotifyBalloonIconType;
    import IWebServiceResponseHandler = Com.Wui.Framework.Commons.Interfaces.IWebServiceResponseHandler;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ObjectEncoder = Com.Wui.Framework.Commons.Utils.ObjectEncoder;
    import ObjectDecoder = Com.Wui.Framework.Commons.Utils.ObjectDecoder;
    import TaskBarProgressState = Com.Wui.Framework.Services.Enums.TaskBarProgressState;
    import IWindowHandlerConnectorEvents = Com.Wui.Framework.Services.Interfaces.Events.IWindowHandlerConnectorEvents;
    import BaseConnector = Com.Wui.Framework.Services.Primitives.BaseConnector;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    /**
     * WindowHandlerConnector class provides wrapping of window manipulation provided by WUI Connector instance.
     */
    export class WindowHandlerConnector extends BaseConnector {
        private readonly subscribers : ArrayList<ArrayList<any>>;
        private readonly pid : number;
        private readonly title : string;

        /**
         * @param {number} $pid Specify process ID, which should be handled
         * @param {string} [$title] Specify Window title in desired process
         */
        constructor($pid? : number, $title? : string) {
            super(true);

            this.subscribers = new ArrayList<ArrayList<any>>();
            if (ObjectValidator.IsSet($pid)) {
                this.pid = Property.Integer(this.pid, $pid);
                this.title = Property.String(this.title, $title);
            }
            this.init();
        }

        /**
         * @return {IWindowHandlerConnectorEvents} Returns events interface connected with connector instance.
         */
        public getEvents() : IWindowHandlerConnectorEvents {
            const superEvents : IWebServiceClientEvents = this.getClient().getEvents();
            this.extendClientEvents(superEvents, "OnNotifyIconClick");
            this.extendClientEvents(superEvents, "OnNotifyIconDoubleClick", "OnNotifyIconDbClick");
            this.extendClientEvents(superEvents, "OnNotifyIconBalloonClick");
            this.extendClientEvents(superEvents, "OnNotifyIconContextItemSelected");
            this.extendClientEvents(superEvents, "OnWindowChanged");
            return <IWindowHandlerConnectorEvents>superEvents;
        }

        /**
         * @param {string} $windowId Specify window identifier.
         * @return {IWindowHandlerStatusPromise} Returns promise interface for handling of window close event.
         */
        public Close($windowId? : string) : IWindowHandlerStatusPromise {
            return this.invoke("Close", $windowId, this.pid, this.title);
        }

        /**
         * @param {string} $url Specify url which should be opened in new window.
         * @param {WindowHandlerOpenOptions} [$options] Specify options, which should be passed to the executed command.
         * @return {IWindowHandlerOpenPromise} Returns promise interface for handling of open event.
         */
        public Open($url : string, $options? : WindowHandlerOpenOptions) : IWindowHandlerOpenPromise {
            const callbacks : any = {
                then($windowId : string, $url? : string, $data? : string) : void {
                    // declare default callback
                }
            };
            this.invoke("Open", $url, $options)
                .Then(($result : IWindowHandlerResultProtocol | boolean) : void => {
                    if (ObjectValidator.IsSet((<IWindowHandlerResultProtocol>$result).type)) {
                        const result : IWindowHandlerResultProtocol = <IWindowHandlerResultProtocol>$result;
                        if (result.type === EventType.ON_COMPLETE) {
                            callbacks.then(result.windowId, result.url, result.data);
                        }
                    }
                });
            return <IWindowHandlerOpenPromise>{
                Then: ($callback : ($windowId : string, $url? : string, $data? : string) => void) : void => {
                    callbacks.then = <any>$callback;
                }
            };
        }

        /**
         * @param {string} $windowId Specify window identifier.
         * @return {IWindowHandlerCookiesPromise} Returns promise interface for handling of get cookies event.
         */
        public getCookies($windowId? : string) : IWindowHandlerCookiesPromise {
            return this.invoke("getCookies", $windowId);
        }

        /**
         * @param {string} $windowId Specify window identifier.
         * @param {WindowHandlerScriptExecuteOptions} $options Specify options, which should be passed to the executed command.
         * @return {IWindowHandlerChangePromise} Returns promise interface for handling of script execute event.
         */
        public ScriptExecute($windowId : string, $options : WindowHandlerScriptExecuteOptions) : IWindowHandlerChangePromise {
            const callbacks : any = {
                onComplete($status : boolean) : any {
                    // declare default callback
                },
                onMessage($message : string) : any {
                    // declare default callback
                },
                onStart() : any {
                    // declare default callback
                }
            };
            if (ObjectValidator.IsFunction($options.script)) {
                $options.script = "(" + Convert.FunctionToString($options.script) + ")();";
            }
            this.invoke("ScriptExecute", $windowId, $options)
                .Then(($result : IWindowHandlerResultProtocol | boolean) : void => {
                    if (ObjectValidator.IsSet((<IWindowHandlerResultProtocol>$result).type)) {
                        const result : IWindowHandlerResultProtocol = <IWindowHandlerResultProtocol>$result;
                        if (result.type === EventType.ON_START) {
                            callbacks.onStart();
                        } else if (result.type === EventType.ON_CHANGE) {
                            callbacks.onMessage(result.data);
                        } else if (result.type === EventType.ON_COMPLETE) {
                            callbacks.onComplete(true);
                        }
                    }
                });
            const promise : IWindowHandlerChangePromise = <IWindowHandlerChangePromise>{
                OnMessage: ($callback : ($message : string) => void) : IWindowHandlerChangePromise => {
                    callbacks.onMessage = $callback;
                    return promise;
                },
                OnStart  : ($callback : () => void) : IWindowHandlerChangePromise => {
                    callbacks.onStart = $callback;
                    return promise;
                },
                Then     : ($callback : ($status : boolean) => void) : void => {
                    callbacks.onComplete = $callback;
                }
            };

            return promise;
        }

        /**
         * @return {IWindowHandlerStatusPromise} Returns promise interface for handling of window minimize event.
         */
        public Minimize() : IWindowHandlerStatusPromise {
            return this.invoke("Minimize", this.pid, this.title);
        }

        /**
         * @return {IWindowHandlerStatusPromise} Returns promise interface for handling of window maximize event.
         */
        public Maximize() : IWindowHandlerStatusPromise {
            return this.invoke("Maximize", this.pid, this.title);
        }

        /**
         * Restore window state to normal state
         * @return {IWindowHandlerStatusPromise} Returns promise interface for handling of window restore event.
         */
        public Restore() : IWindowHandlerStatusPromise {
            return this.invoke("Restore", this.pid, this.title);
        }

        /**
         * @param {number} $x Specify position from top
         * @param {number} $y Specify position from left
         * @return {IWindowHandlerStatusPromise} Returns promise interface for handling of window move event.
         */
        public MoveTo($x : number, $y : number) : IWindowHandlerStatusPromise {
            return this.invoke("MoveTo", $x, $y, this.pid, this.title);
        }

        /**
         * @param {WindowCornerType} $cornerType Specify corner, which should be binded with new size coordinates
         * @param {number} $dx Specify position from top
         * @param {number} $dy Specify position from left
         * @return {IWindowHandlerStatusPromise} Returns promise interface for handling of window move event.
         */
        public Resize($cornerType : WindowCornerType, $dx : number, $dy : number) : IWindowHandlerStatusPromise {
            return this.invoke("Resize", $cornerType, $dx, $dy, this.pid, this.title);
        }

        /**
         * @param {boolean} $status Specify, if window can be resized or not.
         * @return {IWindowHandlerStatusPromise} Returns promise interface for handling of window resize handling event.
         */
        public CanResize($status : boolean) : IWindowHandlerStatusPromise {
            return this.invoke("CanResize", $status);
        }

        /**
         * @param {string} [$windowId] Specify window identifier.
         * @return {IWindowHandlerStatusPromise} Returns promise interface for handling of window existence event.
         */
        public Exists($windowId? : string) : IWindowHandlerStatusPromise {
            return this.invoke("Exists", $windowId, this.pid, this.title);
        }

        /**
         * @param {string} [$windowId] Specify window identifier.
         * @return {IWindowHandlerStatusPromise} Returns promise interface for handling of window show event.
         */
        public Show($windowId? : string) : IWindowHandlerStatusPromise {
            return this.invoke("Show", $windowId);
        }

        /**
         * @param {string} [$windowId] Specify window identifier.
         * @return {IWindowHandlerStatusPromise} Returns promise interface for handling of window hide event.
         */
        public Hide($windowId? : string) : IWindowHandlerStatusPromise {
            return this.invoke("Hide", $windowId);
        }

        /**
         * @param {string} [$windowId] Specify window identifier.
         * @return {IWindowHandlerStatusPromise} Returns promise interface for handling of window state event.
         */
        public getState($windowId? : string) : IWindowHandlerStatePromise {
            return this.invoke("getState", $windowId);
        }

        /**
         * Creates notify icon in the system tray. Method fails if notify icon exists for current root window.
         * @param {IWindowHandlerNotifyIcon} $notifyIcon Specify notify icon options.
         * @return {ILiveContentFormatterPromise} Returns promise interface for handling of window state event.
         */
        public CreateNotifyIcon($notifyIcon : IWindowHandlerNotifyIcon) : IWindowHandlerNotifyPromise {
            const callbacks : any = {
                onComplete($status : boolean) : any {
                    // declare default callback
                },
                onMessage($message : string) : any {
                    // declare default callback
                }
            };
            this.invoke("CreateNotifyIcon", $notifyIcon)
                .Then(($result : IWindowHandlerNotifyProtocol | boolean) : void => {
                    if (ObjectValidator.IsSet((<IWindowHandlerNotifyProtocol>$result).type)) {
                        const result : IWindowHandlerNotifyProtocol = <IWindowHandlerNotifyProtocol>$result;
                        if (result.type === EventType.ON_MESSAGE) {
                            callbacks.onMessage(result.request);
                        }
                    }
                });
            const promise : IWindowHandlerNotifyPromise = <IWindowHandlerNotifyPromise>{
                OnMessage: ($callback : ($message : string) => void) : IWindowHandlerNotifyPromise => {
                    callbacks.onMessage = $callback;
                    return promise;
                },
                Then     : ($callback : ($status : boolean) => void) : void => {
                    callbacks.onComplete = $callback;
                }
            };

            return promise;
        }

        /**
         * Modify notify icon previously created in the system tray. If notify icon is not exists than will be created with current options.
         * @param {IWindowHandlerNotifyIcon} $notifyIcon Specify notify icno options.
         * @return {ILiveContentFormatterPromise} Returns promise interface for handling of window state event.
         */
        public ModifyNotifyIcon($notifyIcon : IWindowHandlerNotifyIcon) : IWindowHandlerStatusPromise {
            return this.invoke("ModifyNotifyIcon", $notifyIcon);
        }

        /**
         * Destroys existing notify icon.
         * @return {ILiveContentFormatterPromise} Returns promise interface for handling of window state event.
         */
        public DestroyNotifyIcon() : IWindowHandlerStatusPromise {
            return this.invoke("DestroyNotifyIcon");
        }

        /**
         * Sets state of TaskBar progress attached with current root window.
         * @param {TaskBarProgressState} $state Specify new progress state.
         * @return {ILiveContentFormatterPromise} Returns promise interface for handling of window state event.
         */
        public setTaskBarProgressState($state : TaskBarProgressState) : IWindowHandlerStatusPromise {
            return this.invoke("setTaskBarProgressState", $state);
        }

        /**
         * Sets progress value of TaskBar attached with current root window.
         * @param {number} $completed Specify completed progress (must be less or equal to $total).
         * @param {number} $total Specify maximal limit of progress.
         * @return {ILiveContentFormatterPromise} Returns promise interface for handling of window state event.
         */
        public setTaskBarProgressValue($completed : number, $total : number) : IWindowHandlerStatusPromise {
            return this.invoke("setTaskBarProgressValue", $completed, $total);
        }

        /**
         * @param {string} $name Specify event name value.
         * @param {IWebServiceResponseHandler} $handler Specify handler, which should be used for handling of event data.
         * @return {void}
         */
        public AddEventListener($name : string, $handler : IWebServiceResponseHandler) : void {
            this.getClient().Send(this.getProtocol("AddEventListener", {
                    name: $name
                }),
                ($data : any) : void => {
                    if ($data.type === "AddEventListener") {
                        if (ObjectDecoder.Base64($data.data) === "true") {
                            if (!this.subscribers.KeyExists($name)) {
                                this.subscribers.Add(new ArrayList<any>(), $name);
                            }
                            this.subscribers.getItem($name).Add($handler, $data.id);
                        }
                    } else if ($data.type === "FireEvent") {
                        const data : any = JSON.parse(ObjectDecoder.Base64($data.data));
                        if (this.subscribers.KeyExists(data.name)) {
                            const handlers : ArrayList<any> = this.subscribers.getItem(data.name);
                            handlers.foreach(($handler : any, $id : number) : void => {
                                if ($id === $data.id) {
                                    $handler.apply(this, JSON.parse(ObjectDecoder.Base64(data.args)));
                                }
                            });
                        }
                    }
                });
        }

        /**
         * @param {string} $name Specify event name value, which should be removed from events pool.
         * @return {void}
         */
        public RemoveEventListener($name : string) : void {
            if (this.subscribers.KeyExists($name)) {
                this.subscribers.RemoveAt(this.subscribers.getKeys().indexOf($name));
            }
        }

        /**
         * Sets progress value of TaskBar attached with current root window.
         * @param {IWindowHandlerFileDialogSettings} $settings Specify file dialog options.
         * @return {IWindowHandlerFileDialogPromise} Returns promise interface for handling of file dialog events.
         */
        public ShowFileDialog($settings : IWindowHandlerFileDialogSettings) : IWindowHandlerFileDialogPromise {
            const callbacks : any = {
                then($status : boolean, $path : string | string[]) : void {
                    // declare default callback
                }
            };
            this.invoke("ShowFileDialog", $settings)
                .Then(($result : boolean, $path : string[]) : void => {
                    if (ObjectValidator.IsArray($path) && $path.length === 1) {
                        callbacks.then($result, $path[0]);
                    } else {
                        callbacks.then($result, $path);
                    }
                });

            return <IWindowHandlerFileDialogPromise>{
                Then: ($callback : ($status : boolean, $path : string | string[]) => void) : void => {
                    callbacks.then = <any>$callback;
                }
            };
        }

        /**
         * Show HTML debug window from CEF instance.
         * @return {IWindowHandlerStatusPromise} Returns promise interface for handling of debug console promise.
         */
        public ShowDebugConsole() : IWindowHandlerStatusPromise {
            return this.invoke("ShowDebugConsole");
        }

        protected getClientType() : WebServiceClientType {
            if (!ObjectValidator.IsEmptyOrNull(this.pid) || !ObjectValidator.IsEmptyOrNull(this.title)) {
                return WebServiceClientType.WUI_DESKTOP_CONNECTOR;
            }
            return WebServiceClientType.CEF_QUERY;
        }

        protected getServerNamespaces() : any {
            const namespaces : any = {};
            namespaces[WebServiceClientType.CEF_QUERY] = "Com.Wui.Framework.ChromiumRE.Connectors.WindowHandler";
            namespaces[WebServiceClientType.WUI_DESKTOP_CONNECTOR] = "Com.Wui.Framework.Connector.Connectors.WindowHandler";
            return namespaces;
        }

        protected init() : void {
            if (ObjectValidator.IsSet(this.subscribers)) {
                super.init();
            }
        }

        private getProtocol($type : string, $data : any) : any {
            return {
                data: ObjectEncoder.Base64(JSON.stringify($data)),
                type: $type
            };
        }

        private extendClientEvents($container : IWebServiceClientEvents, $eventType : string, $interfaceName? : string) : void {
            if (!ObjectValidator.IsSet($interfaceName)) {
                $interfaceName = $eventType;
            }
            $container[$eventType] = ($handler : IWebServiceResponseHandler) : void => {
                this.AddEventListener($interfaceName, $handler);
            };
        }
    }

    export abstract class WindowHandlerOpenOptions {
        public hidden : boolean;
    }

    export abstract class WindowHandlerScriptExecuteOptions {
        public script : string | (() => void);
    }

    export abstract class IWindowHandlerResultProtocol {
        public type : EventType;
        public data : string;
        public url : string;
        public windowId : string;
    }

    export abstract class IWindowHandlerNotifyProtocol {
        public type : EventType;
        public request : string;
    }

    export interface IWindowHandlerOpenPromise {
        Then($callback : ($windowId : string, $url? : string, $data? : string) => void) : void;
    }

    export interface IWindowHandlerChangePromise {
        OnStart($callback : () => void) : IWindowHandlerChangePromise;

        OnMessage($callback : ($message : string) => void) : IWindowHandlerChangePromise;

        Then($callback : ($status : boolean) => void) : void;
    }

    export interface IWindowHandlerStatusPromise {
        Then($callback : ($success : boolean) => void) : void;
    }

    export interface IWindowHandlerStatePromise {
        Then($callback : ($state : WindowStateType) => void) : void;
    }

    export interface IWindowHandlerCookiesPromise {
        Then($callback : ($data : IWindowHandlerCookie[]) => void) : void;
    }

    export interface IWindowHandlerNotifyPromise {
        OnMessage($callback : ($message : string) => void) : IWindowHandlerNotifyPromise;

        Then($callback : ($success : boolean) => void) : void;
    }

    export interface IWindowHandlerFileDialogPromise {
        Then($callback : ($status : boolean, $path : string | string[]) => void) : void;
    }

    export abstract class IWindowHandlerFileDialogSettings {
        public title : string;
        public filter? : string[];
        public filterIndex? : number;
        public path? : string;

        public openOnly? : boolean;
        public folderOnly? : boolean;
        public multiSelect? : boolean;
        public initialDirectory? : string;
    }

    export abstract class IWindowHandlerCookie {
        public path : string;
        public name : string;
        public value : string;
    }

    export abstract class IWindowHandlerNotifyIcon {
        public tip : string;
        public icon? : string;
        public balloon? : IWindowHandlerNotifyBalloon;
        public contextMenu? : IWindowHandlerNotifyIconContextMenu;
    }

    export abstract class IWindowHandlerNotifyBalloon {
        public title : string;
        public message? : string;
        public type? : NotifyBalloonIconType;
        public userIcon? : string;
        public noSound? : boolean;
    }

    export abstract class IWindowHandlerNotifyIconContextMenu {
        public items? : IWindowHandlerNotifyIconContextMenuItem[];
    }

    export abstract class IWindowHandlerNotifyIconContextMenuItem {
        public name : string;
        public label : string;
        public position? : number;
    }
}
