/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    import WebServiceClientType = Com.Wui.Framework.Services.Enums.WebServiceClientType;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BaseConnector = Com.Wui.Framework.Services.Primitives.BaseConnector;

    /**
     * VirtualBoxConnector class provides wrapping of Virtualbox control services.
     */
    export class VirtualBoxConnector extends BaseConnector {

        constructor() {
            super(true);
        }

        /**
         * @param {string} $machineNameOrId Specify name or id of existing VM instance at VirtualBox
         * @param {string} [$user="Guest"] Specify user name, which should be used for login into the VM.
         * @param {string} [$pass=""] Specify password, which should be used for login into the VM.
         * @return {IVirtualBoxStartPromise} Returns promise interface for handling this event.
         */
        public Start($machineNameOrId : string, $user : string = "Guest", $pass : string = "") : IVirtualBoxStartPromise {
            return this.invoke("Start", $machineNameOrId, $user, $pass);
        }

        /**
         * @param {string} $sessionId Specify session id, which should be invoked on VM.
         * @param {string} $sourcePath Specify source file, which should be copied to VM.
         * @param {string} $destinationPath Specify destination path, where should be file copied to VM.
         * @return {IVirtualBoxStatusPromise} Returns promise interface for handling this event.
         */
        public Copy($sessionId : string, $sourcePath : string, $destinationPath : string) : IVirtualBoxStatusPromise {
            return this.invoke("Copy", $sessionId, $sourcePath, $destinationPath);
        }

        /**
         * @param {string} $sessionId Specify session id, which should be invoked on VM.
         * @param {string} $filePath Specify path to file, which should be executed on VM.
         * @return {IVirtualBoxStatusPromise} Returns promise interface for handling this event.
         */
        public Execute($sessionId : string, $filePath : string) : IVirtualBoxStatusPromise {
            return this.invoke("Execute", $sessionId, $filePath);
        }

        protected getClientType() : WebServiceClientType {
            return WebServiceClientType.WUI_BUILDER_CONNECTOR;
        }

        protected getServerNamespaces() : any {
            const namespaces : any = {};
            namespaces[WebServiceClientType.WUI_BUILDER_CONNECTOR] = "Com.Wui.Framework.Builder.Connectors.VirtualBoxHandler";
            return namespaces;
        }
    }

    export interface IVirtualBoxStartPromise {
        Then($callback : ($success : boolean, $sessionId : string) => void) : void;
    }

    export interface IVirtualBoxStatusPromise {
        Then($callback : ($success : boolean) => void) : void;
    }
}
