/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    import LiveContentWrapper = Com.Wui.Framework.Services.WebServiceApi.LiveContentWrapper;
    import WebServiceClientType = Com.Wui.Framework.Services.Enums.WebServiceClientType;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IFileSystemItemProtocol = Com.Wui.Framework.Commons.Interfaces.IFileSystemItemProtocol;
    import ProgressEventArgs = Com.Wui.Framework.Commons.Events.Args.ProgressEventArgs;
    import EventType = Com.Wui.Framework.Commons.Enums.Events.EventType;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import HttpMethodType = Com.Wui.Framework.Commons.Enums.HttpMethodType;
    import IWebServiceProtocol = Com.Wui.Framework.Services.Interfaces.IWebServiceProtocol;
    import BaseConnector = Com.Wui.Framework.Services.Primitives.BaseConnector;
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;

    /**
     * FileSystemHandlerConnector class provides wrapping of local file system services provided by WUI Connector instance.
     */
    export class FileSystemHandlerConnector extends BaseConnector {

        /**
         * @return {IFileSystemFilePromise} Returns promise interface for handling of data receive event.
         */
        public getTempPath() : IFileSystemFilePromise {
            return this.invoke("getTempPath")
                .DataFormatter(($data : string) : string => {
                    return StringUtils.Replace(StringUtils.Replace(StringUtils.Remove($data, "\""), "\\", "/"), "//", "/");
                });
        }

        /**
         * @return {IFileSystemFilePromise} Returns promise interface for handling of data receive event.
         */
        public getWuiHostLocation() : IFileSystemFilePromise {
            return {
                Then: ($callBack : ($path : string) => void) : void => {
                    this.getClient().getServerPath($callBack);
                }
            };
        }

        /**
         * @param {string} [$path] Specify path, which should be looked up.
         * @return {IFileSystemMapPromise} Returns promise interface for handling of data receive event.
         */
        public getPathMap($path? : string) : IFileSystemMapPromise {
            return this.invoke("getPathMap", $path)
                .DataFormatter(($data : IFileSystemItemProtocol[]) : IFileSystemItemProtocol[] => {
                    if (!ObjectValidator.IsArray($data)) {
                        return [];
                    }
                    return $data;
                });
        }

        /**
         * @param {string} [$additionalInfo=false] Specify, if additional network information should be processed e.g. ARP with DNS lookup.
         * @return {IFileSystemMapPromise} Returns promise interface for handling of data receive event.
         */
        public getNetworkMap($additionalInfo : boolean = false) : IFileSystemMapPromise {
            return this.invoke("getNetworkMap", $additionalInfo)
                .DataFormatter(($data : IFileSystemItemProtocol[]) : IFileSystemItemProtocol[] => {
                    if (!ObjectValidator.IsArray($data)) {
                        return [];
                    }
                    return $data;
                });
        }

        /**
         * @param {string} $path Specify path, which should be looked up.
         * @return {IFileSystemMapPromise} Returns promise interface for handling of data receive event.
         */
        public getDirectoryContent($path : string) : IFileSystemMapPromise {
            return this.invoke("getDirectoryContent", $path)
                .DataFormatter(($data : IFileSystemItemProtocol[]) : IFileSystemItemProtocol[] => {
                    if (!ObjectValidator.IsArray($data)) {
                        return [];
                    }
                    return $data;
                });
        }

        /**
         * @param {string|string[]} $pattern Specify pattern for search.
         * @return {IFileSystemExpandPromise} Returns a unique array of all file or directory paths that match the given $pattern.
         */
        public Expand($pattern : string | string[]) : IFileSystemExpandPromise {
            return this.invoke("Expand", $pattern);
        }

        /**
         * @param {string} $path Specify path, which should be validated.
         * @return {IFileSystemStatusPromise} Returns promise interface for handling of existence event.
         */
        public Exists($path : string) : IFileSystemStatusPromise {
            return this.invoke("Exists", $path);
        }

        /**
         * @param {string} $path Specify file path, which should be used as data destination.
         * @param {string} $data Specify data, which should saved.
         * @param {boolean} [$append=false] Specify, if data should be appended to existing data in the file.
         * @param {boolean} [$stream=false] Specify, if data should be consumed as base64 stream.
         * @return {IFileSystemStatusPromise} Returns promise interface for handling of write event.
         */
        public Write($path : string, $data : string, $append : boolean = false, $stream : boolean = false) : IFileSystemStatusPromise {
            return this.invoke(!$stream ? "Write" : "WriteStream", $path, $data, $append);
        }

        /**
         * @param {string} $path Specify file path, which should be read.
         * @param {boolean} [$stream=false] Specify, if output should be in base64 format.
         * @return {IFileSystemFilePromise} Returns promise interface for handling of data receive event.
         */
        public Read($path : string, $stream : boolean = false) : IFileSystemFilePromise {
            if ($stream) {
                return this.invoke("ReadStream", $path);
            } else {
                return this.invoke("Read", $path)
                    .DataFormatter(($data : string) : string => {
                        if (StringUtils.StartsWith($data, "\"") && StringUtils.EndsWith($data, "\"")) {
                            $data = StringUtils.Replace($data, "\\r", "\r");
                            $data = StringUtils.Replace($data, "\\n", "\n");
                            $data = StringUtils.Replace($data, "\r\n", "\n");
                            $data = StringUtils.Replace($data, "\\\"", "\"");
                            return StringUtils.Substring($data, 1, StringUtils.Length($data) - 2);
                        }
                        return $data;
                    });
            }
        }

        /**
         * @param {string} $path Specify path, which should be deleted.
         * @return {IFileSystemChangePromise} Returns promise interface for handling delete event.
         */
        public Delete($path : string) : IFileSystemChangePromise {
            const callbacks : any = {
                onChange($args : ProgressEventArgs) : any {
                    // declare default callback
                },
                onComplete($success : boolean) : any {
                    // declare default callback
                }
            };
            this.invoke("Delete", $path)
                .Then(($result : IFileSystemResultProtocol) : void => {
                    if (ObjectValidator.IsSet($result.type)) {
                        if ($result.type === EventType.ON_CHANGE) {
                            if (ObjectValidator.IsSet($result.data)) {
                                $result = <IFileSystemResultProtocol>$result.data;
                            }
                            const args : ProgressEventArgs = new ProgressEventArgs();
                            args.RangeStart($result.rangeStart);
                            args.RangeEnd($result.rangeEnd);
                            args.CurrentValue($result.currentValue);
                            callbacks.onChange(args);
                        } else if ($result.type === EventType.ON_COMPLETE) {
                            callbacks.onComplete($result.data);
                        }
                    } else if (ObjectValidator.IsBoolean($result)) {
                        callbacks.onComplete($result);
                    }
                });
            const promise : IFileSystemChangePromise = {
                OnChange: ($callback : ($args : ProgressEventArgs) => void) : IFileSystemChangePromise => {
                    callbacks.onChange = $callback;
                    return promise;
                },
                Then    : ($callback : ($success : boolean) => void) : void => {
                    callbacks.onComplete = $callback;
                }
            };
            return promise;
        }

        /**
         * @param {string} $path Specify path, which should be created.
         * @return {IFileSystemStatusPromise} Returns promise interface for handling directory create event.
         */
        public CreateDirectory($path : string) : IFileSystemStatusPromise {
            return this.invoke("CreateDirectory", $path);
        }

        /**
         * @param {string} $oldPath Specify path, which should be renamed.
         * @param {string} $newPath Specify new path value.
         * @return {IFileSystemStatusPromise} Returns promise interface for handling rename event.
         */
        public Rename($oldPath : string, $newPath : string) : IFileSystemStatusPromise {
            return this.invoke("Rename", $oldPath, $newPath);
        }

        /**
         * @param {string} $sourcePath Specify path, which should be used as data source.
         * @param {string} $destinationPath Specify path, where should be data stored.
         * @return {IFileSystemChangePromise} Returns promise interface for handling copy event.
         */
        public Copy($sourcePath : string, $destinationPath : string) : IFileSystemChangePromise {
            const callbacks : any = {
                onChange($args : ProgressEventArgs) : any {
                    // declare default callback
                },
                onComplete($success : boolean) : any {
                    // declare default callback
                }
            };
            this.invoke("Copy", $sourcePath, $destinationPath)
                .Then(($result : IFileSystemResultProtocol) : void => {
                    if (ObjectValidator.IsSet($result.type)) {
                        if ($result.type === EventType.ON_CHANGE) {
                            if (ObjectValidator.IsSet($result.data)) {
                                $result = <IFileSystemResultProtocol>$result.data;
                            }
                            const args : ProgressEventArgs = new ProgressEventArgs();
                            args.RangeStart($result.rangeStart);
                            args.RangeEnd($result.rangeEnd);
                            args.CurrentValue($result.currentValue);
                            callbacks.onChange(args);
                        } else if ($result.type === EventType.ON_COMPLETE) {
                            callbacks.onComplete($result.data);
                        }
                    } else if (ObjectValidator.IsBoolean($result)) {
                        callbacks.onComplete($result);
                    }
                });
            const promise : IFileSystemChangePromise = {
                OnChange: ($callback : ($args : ProgressEventArgs) => void) : IFileSystemChangePromise => {
                    callbacks.onChange = $callback;
                    return promise;
                },
                Then    : ($callback : ($success : boolean) => void) : void => {
                    callbacks.onComplete = $callback;
                }
            };
            return promise;
        }

        /* tslint:disable: unified-signatures */
        /**
         * @param {string} $sourcePath Specify path, which should be used as data source.
         * @return {IFileSystemStatusPromise} Returns promise interface for handling download event.
         */
        public Download($sourcePath : string) : IFileSystemDownloadPromise;

        /**
         * @param {FileSystemDownloadOptions} $options Specify HTTP request options, which should be used for download of data source.
         * @return {IFileSystemStatusPromise} Returns promise interface for handling download event.
         */
        public Download($options : FileSystemDownloadOptions) : IFileSystemDownloadPromise;

        public Download($sourcePathOrOptions : any) : IFileSystemDownloadPromise {
            const callbacks : any = {
                connectionId: -1,
                onChange($args : ProgressEventArgs) : any {
                    // declare default callback
                },
                onComplete($tmpPathOrHeaders : any, $bodyOrHeaders : any) : any {
                    // declare default callback
                },
                onError     : null,
                onStart($connectionId? : number) : any {
                    // declare default callback
                }
            };
            const methodCall : LiveContentWrapper = new LiveContentWrapper(this.getServerNamespace());
            this.getClient().Send(
                methodCall.getProtocolForInvokeMethod.apply(methodCall, [this.getId(), "Download", $sourcePathOrOptions]),
                ($data : IWebServiceProtocol) : void => {
                    callbacks.connectionId = $data.id;
                    (<any>methodCall).getResponseHandler(methodCall, $data);
                });
            methodCall
                .OnError(($error : ErrorEventArgs, ...$args : any[]) : void => {
                    this.errorHandler(callbacks.onError, $error, $args);
                })
                .Then(($result : IFileSystemResultProtocol) : void => {
                    if (!ObjectValidator.IsEmptyOrNull($result) && ObjectValidator.IsSet($result.type)) {
                        if ($result.type === EventType.ON_START) {
                            callbacks.onStart(callbacks.connectionId);
                        } else if ($result.type === EventType.ON_CHANGE) {
                            if (ObjectValidator.IsSet($result.data)) {
                                $result = <IFileSystemResultProtocol>$result.data;
                            }
                            const args : ProgressEventArgs = new ProgressEventArgs();
                            args.RangeStart($result.rangeStart);
                            args.RangeEnd($result.rangeEnd);
                            args.CurrentValue($result.currentValue);
                            callbacks.onChange(args);
                        } else if ($result.type === EventType.ON_COMPLETE) {
                            $result.headers = {};
                            if (ObjectValidator.IsSet($result.data)) {
                                $result.headers = <any>$result.data[0];
                                if (ObjectValidator.IsObject($sourcePathOrOptions) &&
                                    (<FileSystemDownloadOptions>$sourcePathOrOptions).streamOutput) {
                                    $result.body = <string>$result.data[1];
                                } else {
                                    $result.path = <string>$result.data[1];
                                }
                            }
                            const headers : ArrayList<string> = new ArrayList<string>();
                            let parameter : any;
                            for (parameter in $result.headers) {
                                if ($result.headers.hasOwnProperty(parameter)) {
                                    headers.Add($result.headers[parameter], parameter);
                                }
                            }
                            if (ObjectValidator.IsSet($result.path)) {
                                callbacks.onComplete($result.path, headers);
                            } else {
                                callbacks.onComplete(headers, $result.body);
                            }
                        }
                    }
                });

            const promise : IFileSystemDownloadPromise = {
                OnChange  : ($callback : ($args : ProgressEventArgs) => void) : IFileSystemDownloadPromise => {
                    callbacks.onChange = $callback;
                    return promise;
                },
                OnComplete: ($callback : ($tmpPathOrHeaders : any, $bodyOrHeaders : any) => void) : IFileSystemDownloadPromise => {
                    callbacks.onComplete = $callback;
                    return promise;
                },
                OnError   : ($callback : ($error : ErrorEventArgs, ...$args : any[]) => void) : IFileSystemDownloadPromise => {
                    callbacks.onError = $callback;
                    return promise;
                },
                OnStart   : ($callback : ($connectionId? : number) => void) : IFileSystemDownloadPromise => {
                    callbacks.onStart = $callback;
                    return promise;
                },
                Then      : ($callback : ($tmpPathOrHeaders : any, $body? : string) => void) : void => {
                    callbacks.onComplete = $callback;
                }
            };
            return promise;
        }

        /* tslint:enable */

        /**
         * @param {number} $id Specify connection id, which should be closed.
         * @return {IFileSystemStatusPromise} Returns promise interface for handling download abort event.
         */
        public AbortDownload($id : number) : IFileSystemStatusPromise {
            return this.invoke("AbortDownload", $id);
        }

        /**
         * @param {string} $path Specify path, which should be unpacked.
         * @param {ArchiveOptions} [$options] Specify additional options for unpack process.
         * @return {IFileSystemFilePromise} Returns promise interface for handling unpack event.
         */
        public Unpack($path : string, $options? : ArchiveOptions) : IFileSystemFileErrorPromise {
            const callbacks : any = {
                onError: null,
                then($data : string) : any {
                    // declare default callback
                }
            };
            this.invoke("Unpack", $path, $options)
                .OnError(($error : ErrorEventArgs, ...$args : any[]) : void => {
                    this.errorHandler(callbacks.onError, $error, $args);
                })
                .Then(($result : IFileSystemResultProtocol) : void => {
                    if (ObjectValidator.IsSet($result.type) && $result.type === EventType.ON_COMPLETE) {
                        if (ObjectValidator.IsSet($result.path)) {
                            callbacks.then($result.path);
                        } else {
                            callbacks.then(<string>$result.data);
                        }
                    }
                });
            const promise : IFileSystemFileErrorPromise = {
                OnError: ($callback : ($error : ErrorEventArgs, ...$args : any[]) => void) : IFileSystemFilePromise => {
                    callbacks.onError = $callback;
                    return promise;
                },
                Then   : ($callback : ($data : string) => void) : void => {
                    callbacks.then = $callback;
                }
            };
            return promise;
        }

        /**
         * @param {string} $source Specify path, which should be used as shortcut target.
         * @param {string} $destination Specify path, where should be shortcut created.
         * @param {ShortcutOptions} [$options] Specify shortcut details.
         * @return {IFileSystemStatusPromise} Returns promise interface for handling shortcut event.
         */
        public CreateShortcut($source : string, $destination : string, $options? : ShortcutOptions) : IFileSystemStatusErrorPromise {
            const callbacks : any = {
                onError: null,
                then($status : boolean) : any {
                    // declare default callback
                }
            };
            this.invoke("CreateShortcut", $source, $destination, $options)
                .OnError(($error : ErrorEventArgs, ...$args : any[]) : void => {
                    this.errorHandler(callbacks.onError, $error, $args);
                })
                .Then(($status : boolean) : void => {
                    callbacks.then($status);
                });

            const promise : IFileSystemStatusErrorPromise = {
                OnError: ($callback : ($error : ErrorEventArgs, ...$args : any[]) => void) : IFileSystemStatusPromise => {
                    callbacks.onError = $callback;
                    return promise;
                },
                Then   : ($callback : ($status : boolean) => void) : void => {
                    callbacks.then = $callback;
                }
            };
            return promise;
        }

        /**
         * @param {string} $path Specify link path, which should be resolved to target path.
         * @return {IFileSystemStatusPromise} Returns promise interface for handling of write event.
         */
        public getLinkTargetPath($path : string) : IFileSystemFilePromise {
            return this.invoke("getLinkTargetPath", $path);
        }

        /**
         * Validate, if Directory or File is empty
         * @param {string} $path Specify path, which should be validated.
         * @return {IFileSystemStatusPromise} Returns promise interface for handling this event.
         */
        public IsEmpty($path : string) : IFileSystemStatusPromise {
            return this.invoke("IsEmpty", $path);
        }

        /**
         * @param {string} $path Specify path, which should be packed.
         * @param {ArchiveOptions} [$options] Specify additional options for pack process.
         * @return {IFileSystemFilePromise} Returns promise interface for handling pack event.
         */
        public Pack($path : string, $options? : ArchiveOptions) : IFileSystemFileErrorPromise {
            const callbacks : any = {
                onError: null,
                then($data : string) : any {
                    // declare default callback
                }
            };
            this.invoke("Pack", $path, $options)
                .OnError(($error : ErrorEventArgs, ...$args : any[]) : void => {
                    this.errorHandler(callbacks.onError, $error, $args);
                })
                .Then(($data : string) : void => {
                    callbacks.then($data);
                });
            const promise : IFileSystemFileErrorPromise = {
                OnError: ($callback : ($error : ErrorEventArgs, ...$args : any[]) => void) : IFileSystemFilePromise => {
                    callbacks.onError = $callback;
                    return promise;
                },
                Then   : ($callback : ($data : string) => void) : void => {
                    callbacks.then = $callback;
                }
            };
            return promise;
        }

        /**
         * @param {string} $path Specify path, which should be validated.
         * @return {IFileSystemStatusPromise} Returns promise interface for handling this event.
         */
        public IsFile($path : string) : IFileSystemStatusPromise {
            return this.invoke("IsFile", $path);
        }

        protected getServerNamespaces() : any {
            const namespaces : any = {};
            namespaces[WebServiceClientType.WUI_DESKTOP_CONNECTOR] = "Com.Wui.Framework.Connector.Connectors.FileSystemHandler";
            namespaces[WebServiceClientType.WUI_BUILDER_CONNECTOR] = "Com.Wui.Framework.Builder.Connectors.FileSystemHandler";
            namespaces[WebServiceClientType.WUI_FORWARD_CLIENT] = "Com.Wui.Framework.Connector.Connectors.FileSystemHandler";
            namespaces[WebServiceClientType.WUI_HOST_CLIENT] = "Com.Wui.Framework.Connector.Connectors.FileSystemHandler";
            return namespaces;
        }
    }

    export interface IFileSystemMapPromise {
        Then($callback : ($map : IFileSystemItemProtocol[]) => void) : void;
    }

    export interface IFileSystemStatusErrorPromise {
        OnError($callback : ($error : ErrorEventArgs, ...$args : any[]) => void) : IFileSystemStatusPromise;

        Then($callback : ($success : boolean) => void) : void;
    }

    export interface IFileSystemStatusPromise {
        Then($callback : ($success : boolean) => void) : void;
    }

    export interface IFileSystemFileErrorPromise {
        OnError($callback : ($error : ErrorEventArgs, ...$args : any[]) => void) : IFileSystemFilePromise;

        Then($callback : ($data : string) => void) : void;
    }

    export interface IFileSystemFilePromise {
        Then($callback : ($data : string) => void) : void;
    }

    export interface IFileSystemChangePromise {
        OnChange($callback : ($args : ProgressEventArgs) => void) : IFileSystemChangePromise;

        Then($callback : ($success : boolean) => void) : void;
    }

    export interface IFileSystemDownloadPromise {
        OnStart($callback : ($connectionId? : number) => void) : IFileSystemDownloadPromise;

        OnChange($callback : ($args : ProgressEventArgs) => void) : IFileSystemDownloadPromise;

        /* tslint:disable: unified-signatures */
        OnComplete($callback : ($tmpPath : string, $headers : ArrayList<string>) => void) : IFileSystemDownloadPromise;

        OnComplete($callback : ($headers : ArrayList<string>, $body : string) => void) : IFileSystemDownloadPromise;

        OnError($callback : ($error : ErrorEventArgs, ...$args : any[]) => void) : IFileSystemDownloadPromise;

        Then($callback : ($tmpPath : string) => void) : void;

        Then($callback : ($headers : ArrayList<string>, $body : string) => void) : void;

        /* tslint:enable */
    }

    export interface IFileSystemExpandPromise {
        Then($callback : ($paths : string[]) => void) : void;
    }

    export abstract class IFileSystemResultProtocol {
        public type : EventType;
        public rangeStart : number;
        public rangeEnd : number;
        public currentValue : number;
        public path : string;
        public headers : any;
        public body : string;
        public data? : string | IFileSystemResultProtocol;
    }

    export abstract class FileSystemDownloadOptions {
        public method? : HttpMethodType;
        public url : string;
        public headers? : any;
        public body? : string;
        public streamOutput? : boolean;
        public proxy? : boolean;
        public streamReturnsBuffer? : boolean;
    }

    export enum ShortcutRunStyle {
        NORMAL = 1,
        MAX = 3,
        MIN = 7
    }

    export abstract class ShortcutOptions {
        public args? : string[];
        public workingDir? : string;
        public runStyle? : ShortcutRunStyle;
        public icon? : string;
        public iconIndex? : number;
        public hotkey? : number;
        public desc? : string;
    }

    export abstract class ArchiveOptions {
        public cwd? : string;
        public output? : string;
        public type? : string;
        public autoStrip? : boolean;
        public override? : boolean;
    }
}
