/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    import WebServiceClientType = Com.Wui.Framework.Services.Enums.WebServiceClientType;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import LiveContentWrapper = Com.Wui.Framework.Services.WebServiceApi.LiveContentWrapper;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ObjectEncoder = Com.Wui.Framework.Commons.Utils.ObjectEncoder;
    import BaseConnector = Com.Wui.Framework.Services.Primitives.BaseConnector;

    /**
     * ReportServiceConnector class provides wrapping of Reporting services provided by WUI Connector servers.
     */
    export class ReportServiceConnector extends BaseConnector {

        /**
         * @param {string} $message Specify message, which should be logged.
         * @param {...any[]} [$args] Specify args, for message in formatter type.
         * @return {ILogItPromise} Returns promise interface for handling of log event.
         */
        public LogIt($message : string, ...$args : any[]) : ILogItPromise {
            return this.invoke("LogIt", ObjectEncoder.Base64(StringUtils.Format($message, $args), true));
        }

        /**
         * @param {string|string[]} $to Specify email receiver as string splitted by ',' or in array format.
         * @param {string} $subject Specify subject text.
         * @param {string} $body Specify email body in pain-text/html format or as path to email template file.
         * @param {string[]} [$attachments] Specify email attachments.
         * @param {string} [$plainText] Specify alternate text for clients without HTML support.
         * @param {string|string[]} [$copyTo] Specify email copy receiver as string splitted by ',' or in array format.
         * @param {string} [$from] Specify email sender.
         * @return {IAcknowledgePromise} Returns promise interface for handling of send mail event.
         */
        public SendMail($to : string | string[], $subject : string, $body : string,
                        $attachments : string[] = null, $plainText : string = "", $copyTo : string | string[] = "",
                        $from : string = "") : IAcknowledgePromise {
            return LiveContentWrapper.InvokeMethod(this.getClient(),
                "Com.Wui.Framework.Hub.Utils.SendMail", "Send",
                $to,
                $subject,
                ObjectEncoder.Base64($body, true),
                $attachments,
                ObjectEncoder.Base64($plainText),
                $copyTo,
                $from);
        }

        /**
         * @param {IReportProtocol} $data Specify data, which should be reported.
         * @return {ICreateReportPromise} Returns promise interface for handling of create report event.
         */
        public CreateReport($data : IReportProtocol) : ICreateReportPromise {
            $data.appName = ObjectEncoder.Base64($data.appName, true);
            $data.appVersion = ObjectEncoder.Base64($data.appVersion, true);
            $data.trace = ObjectEncoder.Base64($data.trace, true);
            $data.log = ObjectEncoder.Base64($data.log, true);
            return this.invoke("CreateReport", $data, true);
        }

        protected getClientType() : WebServiceClientType {
            return WebServiceClientType.WUI_HUB_CONNECTOR;
        }

        protected getServerNamespaces() : any {
            const namespaces : any = {};
            namespaces[WebServiceClientType.WUI_HUB_CONNECTOR] = "Com.Wui.Framework.Hub.Utils.ReportAPI";
            return namespaces;
        }
    }

    export interface ILogItPromise extends Com.Wui.Framework.Services.Interfaces.ILiveContentErrorPromise {
        Then($callback : () => void) : void;
    }

    export interface IAcknowledgePromise extends Com.Wui.Framework.Services.Interfaces.ILiveContentErrorPromise {
        Then($callback : ($success : boolean) => void) : void;
    }

    export abstract class IReportProtocol {
        public appName : string;
        public appVersion : string;
        public appId : string;
        public trace : string;
        public timeStamp : number;
        public printScreen : string;
        public log : string;
    }

    export interface ICreateReportPromise {
        Then($callback : ($success : boolean) => void) : void;
    }
}
