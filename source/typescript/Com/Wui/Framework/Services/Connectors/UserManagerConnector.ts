/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    import WebServiceClientType = Com.Wui.Framework.Services.Enums.WebServiceClientType;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import BaseConnector = Com.Wui.Framework.Services.Primitives.BaseConnector;

    export class UserManagerConnector extends BaseConnector {

        public LogIn($username : string, $password : string) : ILogInPromise {
            return this.invoke("LogIn", $username, StringUtils.getSha1($password))
                .DataFormatter(($data : string) : string => {
                    return StringUtils.Substring($data, 1, StringUtils.Length($data) - 1);
                });
        }

        public LogOut($token : string) : IUserManagerAcknowledgePromise {
            return this.invoke("LogOut", $token);
        }

        public Register($username : string, $password : string) : IUserManagerAcknowledgePromise {
            return this.invoke("Register", $username, StringUtils.getSha1($password));
        }

        public IsAuthenticated($token : string) : IUserManagerAcknowledgePromise {
            return this.invoke("IsAuthenticated", $token);
        }

        public setRole($username : string, $role : string) : IUserManagerAcknowledgePromise {
            return this.invoke("setRole", $username, $role);
        }

        protected getClientType() : WebServiceClientType {
            return WebServiceClientType.WUI_HUB_CONNECTOR;
        }

        protected getServerNamespaces() : any {
            const namespaces : any = {};
            namespaces[WebServiceClientType.WUI_HUB_CONNECTOR] = "Com.Wui.Framework.Hub.Utils.UserManager";
            return namespaces;
        }
    }

    export interface ILogInPromise {
        Then($callback : ($token : string) => void) : void;
    }

    export interface IUserManagerAcknowledgePromise {
        Then($callback : ($status : boolean) => void) : void;
    }
}
