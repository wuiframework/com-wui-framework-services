/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import WebServiceClientType = Com.Wui.Framework.Services.Enums.WebServiceClientType;
    import BaseConnector = Com.Wui.Framework.Services.Primitives.BaseConnector;

    /**
     * TerminalConnector class provides wrapping of local file system services provided by WUI Connector instance.
     */
    export class TerminalConnector extends BaseConnector {

        /**
         * Create new child process and wait for process end
         * @param {string} $command Specify command, which should be executed.
         * @param {string[]} [$args] Specify command args, which should be passed to the executed process.
         * @param {string} [$cwdOrOptions] Specify current working directory for process execution or compact terminal options.
         * @return {IProcessPromise} Returns promise API connected with response on process exit.
         */
        public Execute($command : string, $args? : string[], $cwdOrOptions? : string | TerminalOptions) : IProcessPromise {
            return this.getProcessProtocol("Execute", $command,
                ObjectValidator.IsEmptyOrNull($args) ? [] : $args, $cwdOrOptions);
        }

        /**
         * Create new child process and provide async stdout/stderr by OnMessage event
         * @param {string} $command Specify command, which should be executed.
         * @param {string[]} [$args] Specify command args, which should be passed to the executed process.
         * @param {string} [$cwdOrOptions] Specify current working directory for process execution or compact terminal options.
         * @return {IProcessPromise} Returns promise API connected with response on process exit.
         */
        public Spawn($command : string, $args? : string[], $cwdOrOptions? : string | TerminalOptions) : IProcessPromise {
            return this.getProcessProtocol("Spawn", $command,
                ObjectValidator.IsEmptyOrNull($args) ? [] : $args, $cwdOrOptions);
        }

        /**
         * @param {string} $path Specify path, which should be opened.
         * @return {IOpenPromise} Returns promise API connected with terminal response.
         */
        public Open($path : string) : IOpenPromise {
            return this.invoke("Open", $path)
                .DataFormatter(this.getDataFormatter);
        }

        /**
         * Create new elevated child process and wait for process end
         * @param {string} $command Specify command, which should be executed.
         * @param {string[]} [$args] Specify command args, which should be passed to the executed process.
         * @param {string} [$cwdOrOptions] Specify current working directory for process execution or compact terminal options.
         * @return {IProcessPromise} Returns promise API connected with response on process exit.
         */
        public Elevate($command : string, $args? : string[], $cwdOrOptions? : string | TerminalOptions) : IProcessPromise {
            return this.getProcessProtocol("Elevate", $command, ObjectValidator.IsEmptyOrNull($args) ? [] : $args, $cwdOrOptions);
        }

        /**
         * Kill process on specified path
         * @param {string} $path Specify path where should be process located.
         * @return {IAcknowledgePromise} Returns promise API connected with response on process kill.
         */
        public Kill($path : string) : IAcknowledgePromise {
            return this.invoke("Kill", $path);
        }

        protected getServerNamespaces() : any {
            const namespaces : any = {};
            namespaces[WebServiceClientType.WUI_DESKTOP_CONNECTOR] = "Com.Wui.Framework.Connector.Connectors.Terminal";
            namespaces[WebServiceClientType.WUI_BUILDER_CONNECTOR] = "Com.Wui.Framework.Builder.Connectors.Terminal";
            namespaces[WebServiceClientType.WUI_FORWARD_CLIENT] = "Com.Wui.Framework.Connector.Connectors.Terminal";
            namespaces[WebServiceClientType.WUI_HOST_CLIENT] = "Com.Wui.Framework.Connector.Connectors.Terminal";
            return namespaces;
        }

        private getDataFormatter($data : any) : any {
            if ($data === "null") {
                return null;
            } else {
                if (!ObjectValidator.IsEmptyOrNull($data) && ObjectValidator.IsSet($data.type)) {
                    return $data.data;
                } else {
                    return $data;
                }
            }
        }

        private getProcessProtocol($methodName : string, $command : string, $args? : string[],
                                   $cwdOrOptions? : string | TerminalOptions) : IProcessPromise {
            const callbacks : any = {
                OnExit($exitCode : number, $stdout? : string, $stderr? : string) : void {
                    // declare default callback
                },
                OnMessage($data : string) : void {
                    // declare default callback
                }
            };
            let options : TerminalOptions = {cwd: ""};
            if (!ObjectValidator.IsEmptyOrNull($cwdOrOptions)) {
                if (ObjectValidator.IsString($cwdOrOptions)) {
                    options = {cwd: <string>$cwdOrOptions};
                } else {
                    options = <TerminalOptions>$cwdOrOptions;
                }
            }

            this.invoke($methodName, $command, $args, options)
                .DataFormatter(this.getDataFormatter)
                .Then(($returnValue : number | string, $stdout? : string, $stderr? : string) : void => {
                    if (ObjectValidator.IsArray($returnValue)) {
                        $stdout = $returnValue[1][0];
                        $stderr = $returnValue[1][1];
                        $returnValue = $returnValue[0];
                    }
                    if ($returnValue === null || ObjectValidator.IsInteger($returnValue)) {
                        callbacks.OnExit(<number>$returnValue, $stdout, $stderr);
                    } else if (!ObjectValidator.IsEmptyOrNull($returnValue)) {
                        callbacks.OnMessage(<string>$returnValue);
                    }
                });
            const promise : IProcessPromise = <IProcessPromise>{
                OnMessage($callback : ($data : string) => void) : IProcessPromise {
                    callbacks.OnMessage = $callback;
                    return promise;
                },
                Then($callback : ($exitCode : number, $stdout? : string, $stderr? : string) => void) : void {
                    callbacks.OnExit = $callback;
                }
            };
            return promise;
        }
    }

    export abstract class AdvancedTerminalOptions {
        public colored? : boolean;
        public noTerminalLog? : boolean;
        public noExitOnStderr? : boolean;
        public useStdIn? : boolean;
    }

    export abstract class TerminalOptions {
        public cwd? : string;
        public env? : any;
        public shell? : boolean;
        public verbose? : boolean;
        public detached? : boolean;
        public advanced? : AdvancedTerminalOptions;
    }

    export interface IProcessPromise {
        OnMessage($callback : ($data : string) => void) : IProcessPromise;

        Then($callback : ($exitCode : number, $stdout? : string, $stderr? : string) => void) : void;
    }

    export interface IOpenPromise {
        Then($callback : ($status : boolean) => void) : void;
    }
}
