/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    import LiveContentWrapper = Com.Wui.Framework.Services.WebServiceApi.LiveContentWrapper;
    import WebServiceClientType = Com.Wui.Framework.Services.Enums.WebServiceClientType;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import BaseConnector = Com.Wui.Framework.Services.Primitives.BaseConnector;

    /**
     * IdeHandlerConnector class provides wrapping of IDE manipulation provided by Jxbrowser bridge.
     */
    export class IdeHandlerConnector extends BaseConnector {

        /**
         * @return {IIdeHandlerFileChooserPromise} Returns promise interface for handling of open file chooser event.
         */
        public OpenFileChooser($path? : string, $extensions? : string[]) : IIdeHandlerFileChooserPromise {
            const path : string = ObjectValidator.IsSet($path) ? $path : null;
            const extensions : string[] = ObjectValidator.IsSet($extensions) ? $extensions : null;
            return this.invoke("OpenFileChooser", path, extensions, this.getId());
        }

        /**
         * @return {IIdeHandlerSuccessPromise} Returns promise interface for handling of open info dialog event.
         */
        public OpenInfoDialog($title : string, $message : string) : IIdeHandlerSuccessPromise {
            return this.invoke("OpenInfoDialog", $title, $message, this.getId());
        }

        /**
         * @return {IIdeHandlerPathPromise} Returns promise interface for handling of path info dialog event.
         */
        public getWorkspaceRoot() : IIdeHandlerPathPromise {
            return this.invoke("getWorkspaceRoot", this.getId());
        }

        /**
         * @param {string} $name Specify event name value.
         * @param {IWebServiceResponseHandler} [$handler] Specify handler, which should be used for handling of event data.
         * @return {void}
         */
        public AddEventListener($name : string, $handler : (...$args : any[]) => void) : void {
            LiveContentWrapper.AddEventHandler(this.getClient(), this.getServerNamespaces(), $name, (...$args : any[]) : void => {
                $handler.apply(this, $args);
            });
        }

        protected getClientType() : WebServiceClientType {
            return WebServiceClientType.JXBROWSER_BRIDGE;
        }

        protected getServerNamespaces() : any {
            const namespaces : any = {};
            namespaces[WebServiceClientType.JXBROWSER_BRIDGE] = "Com.Wui.Framework.IdeJRE.Connectors.IdeHandler";
            return namespaces;
        }
    }

    export interface IIdeHandlerFileChooserPromise {
        Then($callback : ($paths : string[]) => void) : void;
    }

    export interface IIdeHandlerPathPromise {
        Then($callback : ($path : string) => void) : void;
    }

    export interface IIdeHandlerSuccessPromise {
        Then($callback : ($success : boolean) => void) : void;
    }
}
