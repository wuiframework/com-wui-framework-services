/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import WebServiceClientType = Com.Wui.Framework.Services.Enums.WebServiceClientType;
    import BaseConnector = Com.Wui.Framework.Services.Primitives.BaseConnector;

    /**
     * NetworkingConnector class provides wrapping of networking services provided by WUI Connector instance.
     */
    export class NetworkingConnector extends BaseConnector {

        /**
         * @return {INetworkingConnectorPortPromise} Returns promise interface for handling of free port.
         */
        public getFreePort() : INetworkingConnectorPortPromise {
            return this.invoke("getFreePort");
        }

        protected getServerNamespaces() : any {
            const namespaces : any = {};
            namespaces[WebServiceClientType.WUI_DESKTOP_CONNECTOR] = "Com.Wui.Framework.Connector.Connectors.Network";
            namespaces[WebServiceClientType.WUI_BUILDER_CONNECTOR] = "Com.Wui.Framework.Builder.Connectors.Network";
            namespaces[WebServiceClientType.WUI_FORWARD_CLIENT] = "Com.Wui.Framework.Connector.Connectors.Network";
            return namespaces;
        }
    }

    export interface INetworkingConnectorPortPromise {
        Then($callback : ($port : number) => void) : void;
    }
}
