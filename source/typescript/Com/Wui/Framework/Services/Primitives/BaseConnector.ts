/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Services.Primitives {
    "use strict";
    import LiveContentWrapper = Com.Wui.Framework.Services.WebServiceApi.LiveContentWrapper;
    import IWebServiceClient = Com.Wui.Framework.Services.Interfaces.IWebServiceClient;
    import WebServiceClientFactory = Com.Wui.Framework.Services.WebServiceApi.WebServiceClientFactory;
    import WebServiceClientType = Com.Wui.Framework.Services.Enums.WebServiceClientType;
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IWebServiceClientEvents = Com.Wui.Framework.Commons.Interfaces.Events.IWebServiceClientEvents;
    import HttpManager = Com.Wui.Framework.Commons.HttpProcessor.HttpManager;
    import WebServiceConfiguration = Com.Wui.Framework.Commons.WebServiceApi.WebServiceConfiguration;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import HttpRequestParser = Com.Wui.Framework.Commons.HttpProcessor.HttpRequestParser;
    import ForwardingClient = Com.Wui.Framework.Services.WebServiceApi.Clients.ForwardingClient;
    import EventsManager = Com.Wui.Framework.Commons.Events.EventsManager;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import WebSocketsClient = Com.Wui.Framework.Commons.WebServiceApi.Clients.WebSocketsClient;
    import ILiveContentFormatterPromise = Com.Wui.Framework.Services.Interfaces.ILiveContentFormatterPromise;
    import WebServiceClientEventType = Com.Wui.Framework.Commons.Enums.Events.WebServiceClientEventType;

    /**
     * BaseConnector class provides generic entry point for back-end connectors.
     */
    export abstract class BaseConnector extends Com.Wui.Framework.Commons.Primitives.BaseObject {
        protected httpManager : HttpManager;
        private serverClassName : string;
        private client : IWebServiceClient;
        private readonly maxReconnectCount : number;
        private propagateError : boolean;

        /**
         * @param {boolean|number} [$reconnect = false] Specify default behaviour on client timeout.
         * If number is passed it will be used as max reconnect limit.
         * @param {string|WebServiceConfiguration} [$serverConfigurationSource] Specify source for configuration of
         * server-client communication.
         * @param {WebServiceClientType} [$clientType] Specify client type.
         */
        constructor($reconnect : boolean | number = false, $serverConfigurationSource? : string | WebServiceConfiguration,
                    $clientType? : WebServiceClientType) {
            super();
            if (ObjectValidator.IsBoolean($reconnect)) {
                this.maxReconnectCount = $reconnect ? -1 : 0;
            } else {
                this.maxReconnectCount = <number>$reconnect;
            }
            this.httpManager = Loader.getInstance().getHttpManager();
            this.propagateError = false;
            this.init($serverConfigurationSource, $clientType);
        }

        /**
         * @return {number} Returns connector id suitable for events handling and connectors factory.
         */
        public getId() : number {
            return this.client.getId();
        }

        /**
         * @return {IWebServiceClientEvents} Returns events interface connected with connector instance.
         */
        public getEvents() : IWebServiceClientEvents {
            return this.client.getEvents();
        }

        /**
         * @param {string} $name Specify name of remote agent which should be looked up by WUI Hub.
         * @param {string|WebServiceConfiguration} [$serverConfigurationSource] Specify source for configuration of
         * server-client communication.
         * @return {void}
         */
        public setAgent($name : string, $serverConfigurationSource? : string | WebServiceConfiguration) : void {
            EventsManager.getInstanceSingleton().Clear("" + this.getId());
            this.client = null;
            if (ObjectValidator.IsString($serverConfigurationSource) &&
                !StringUtils.EndsWith(<string>$serverConfigurationSource, "/connector.config.jsonp")) {
                $serverConfigurationSource += "/connector.config.jsonp";
            }
            this.init($serverConfigurationSource, WebServiceClientType.WUI_FORWARD_CLIENT);
            (<ForwardingClient>this.getClient()).AgentName($name);
        }

        /**
         * @param {boolean} $value Specify if connector should treat error as global exception or not.
         * @return {boolean} Returns true if error should trows exception otherwise false.
         */
        public ErrorPropagation($value? : boolean) : boolean {
            return this.propagateError = Property.Boolean(this.propagateError, $value);
        }

        protected getClientType() : WebServiceClientType {
            let type : WebServiceClientType;
            const request : HttpRequestParser = this.httpManager.getRequest();
            if (!request.IsIdeaHost()) {
                if (request.IsWuiHost()) {
                    type = WebServiceClientType.WUI_HOST_CLIENT;
                } else if (request.IsWuiJre() || request.IsWuiPlugin() || request.IsWuiConnector() || request.IsWuiConnector(true)) {
                    type = WebServiceClientType.WUI_DESKTOP_CONNECTOR;
                } else if (request.IsOnServer() || request.IsOnServer(true)) {
                    type = WebServiceClientType.WUI_HUB_CONNECTOR;
                }
            }
            if (ObjectValidator.IsEmptyOrNull(type)) {
                type = WebServiceClientType.WUI_BUILDER_CONNECTOR;
            }
            return type;
        }

        protected getServerNamespaces() : any {
            const namespaces : any = {};
            namespaces[WebServiceClientType.WUI_DESKTOP_CONNECTOR] = "";
            namespaces[WebServiceClientType.WUI_HUB_CONNECTOR] = "";
            namespaces[WebServiceClientType.WUI_BUILDER_CONNECTOR] = "";
            return namespaces;
        }

        protected init($serverConfigurationSource? : string | WebServiceConfiguration, $clientType? : WebServiceClientType) : void {
            const type : WebServiceClientType = ObjectValidator.IsEmptyOrNull($clientType) ? this.getClientType() : $clientType;
            const namespaces : any = this.getServerNamespaces();
            if (namespaces.hasOwnProperty(<string>type)) {
                this.serverClassName = namespaces[<string>type];
                if (!ObjectValidator.IsEmptyOrNull(this.serverClassName)) {
                    this.client = WebServiceClientFactory.getClient(type, $serverConfigurationSource);
                }
            }
            if (!ObjectValidator.IsEmptyOrNull(this.client)) {
                if (this.maxReconnectCount === -1) {
                    const reconnectHandler : any = () : void => {
                        this.client.StartCommunication();
                    };
                    this.client.getEvents().OnClose(reconnectHandler);
                    this.client.getEvents().OnTimeout(reconnectHandler);
                } else if (this.maxReconnectCount >= 0 && this.client.IsMemberOf(WebSocketsClient)) {
                    (<WebSocketsClient>this.client).MaxReconnectsCount(this.maxReconnectCount);
                }
                const onErrorHandler : any = ($eventArgs : ErrorEventArgs) : void => {
                    if (!this.propagateError && Loader.getInstance().getEnvironmentArgs().IsProductionMode()) {
                        ExceptionsManager.Throw(this.getClassName(), $eventArgs.Exception());
                    } else {
                        LogIt.Warning($eventArgs.Exception().ToString("", false));
                    }
                };
                if ((type === WebServiceClientType.WUI_REST_CONNECTOR || type === WebServiceClientType.WUI_HUB_CONNECTOR) &&
                    this.maxReconnectCount === 0) {
                    const timeoutHandler : any = () : void => {
                        const data : ArrayList<any> = new ArrayList();
                        data.Add(this.getClassName(), "Owner");
                        data.Add(this.client, "Client");
                        this.httpManager.ReloadTo("/ServerError/Http/ConnectionLost", data, true);
                    };
                    this.client.getEvents().OnTimeout(timeoutHandler);
                    this.client.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
                        this.httpManager.IsOnline(($status : boolean) : void => {
                            if ($status) {
                                onErrorHandler($eventArgs);
                            } else {
                                timeoutHandler();
                            }
                        });
                    });
                } else {
                    this.client.getEvents().OnError(onErrorHandler);
                }
            } else {
                ExceptionsManager.Throw(this.getClassName(),
                    "Client for required service is not supported by runtime environment.");
            }
        }

        protected getClient() : IWebServiceClient {
            return this.client;
        }

        protected getServerNamespace() : string {
            return this.serverClassName;
        }

        protected invoke($method : string, ...$args : any[]) : ILiveContentFormatterPromise {
            return LiveContentWrapper.InvokeMethod.apply(LiveContentWrapper,
                (<any[]>[this.client, this.serverClassName, $method]).concat($args));
        }

        protected errorHandler($callback : () => void, $error : ErrorEventArgs, $args : any[]) : void {
            if (ObjectValidator.IsEmptyOrNull($callback)) {
                EventsManager.getInstanceSingleton().FireEvent("" + this.getId(), WebServiceClientEventType.ON_ERROR, $error);
            } else {
                $callback.apply(this, [$error].concat($args));
            }
        }
    }
}
