/** WARNING: this file has been automatically generated from typescript interfaces, which exist in this package. */

/* tslint:disable: variable-name no-use-before-declare only-arrow-functions */
namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    export let IAgentsListPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    export let ICertsConnectorPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    export let ICOMproxyVoidPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    export let ICOMproxyBooleanPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    export let ICOMproxyInstancePromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    export let ICOMproxyMethodPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    export let ICOMproxyPropertyPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    export let IFFIProxyMethodPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    export let IFileSystemMapPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    export let IFileSystemStatusErrorPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "OnError",
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    export let IFileSystemStatusPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    export let IFileSystemFileErrorPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "OnError",
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    export let IFileSystemFilePromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    export let IFileSystemChangePromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "OnChange",
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    export let IFileSystemDownloadPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "OnStart",
                "OnChange",
                "OnComplete",
                "OnError",
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    export let IFileSystemExpandPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    export let IIdeHandlerFileChooserPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    export let IIdeHandlerPathPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    export let IIdeHandlerSuccessPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    export let INetworkingConnectorPortPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    export let ICreateReportPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    export let IProcessPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "OnMessage",
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    export let IOpenPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    export let ILogInPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    export let IUserManagerAcknowledgePromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    export let IVirtualBoxStartPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    export let IVirtualBoxStatusPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    export let IWindowHandlerOpenPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    export let IWindowHandlerChangePromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "OnStart",
                "OnMessage",
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    export let IWindowHandlerStatusPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    export let IWindowHandlerStatePromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    export let IWindowHandlerCookiesPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    export let IWindowHandlerNotifyPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "OnMessage",
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    export let IWindowHandlerFileDialogPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Interfaces.DAO {
    "use strict";
    export let IInstallationHandler : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Interfaces.DAO {
    "use strict";
    export let IInstallationUtilValuePromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Interfaces.DAO {
    "use strict";
    export let IInstallationUtilAcknowledgePromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Interfaces.DAO {
    "use strict";
    export let IInstallationUtilStatusPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Interfaces.DAO {
    "use strict";
    export let IInstallationUtilStatusOrValuePromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Interfaces.DAO {
    "use strict";
    export let IInstallationUtilTerminalPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Interfaces {
    "use strict";
    export let ILiveContentFormatterPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "DataFormatter",
                "OnError",
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Interfaces {
    "use strict";
    export let ILiveContentErrorPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "OnError",
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Interfaces {
    "use strict";
    export let ILiveContentPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Then"
            ]);
        }();
}

namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    export let IAcknowledgePromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Then"
            ], Com.Wui.Framework.Services.Interfaces.ILiveContentErrorPromise);
        }();
}

namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    export let IAgentsRegisterPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "OnMessage"
            ], Com.Wui.Framework.Services.Connectors.IAcknowledgePromise);
        }();
}

namespace Com.Wui.Framework.Services.Connectors {
    "use strict";
    export let ILogItPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Then"
            ], Com.Wui.Framework.Services.Interfaces.ILiveContentErrorPromise);
        }();
}

namespace Com.Wui.Framework.Services.Interfaces.Events {
    "use strict";
    export let IInstallationProcessEventsHandler : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ], Com.Wui.Framework.Commons.Interfaces.IEventsHandler);
        }();
}

namespace Com.Wui.Framework.Services.Interfaces.Events {
    "use strict";
    export let IInstallationRecipeEvents : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "setOnStart",
                "setOnChange",
                "setOnComplete",
                "setOnError"
            ], Com.Wui.Framework.Commons.Interfaces.IBaseObject);
        }();
}

namespace Com.Wui.Framework.Services.Interfaces.Events {
    "use strict";
    export let IWindowHandlerConnectorEvents : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "OnNotifyIconClick",
                "OnNotifyIconDoubleClick",
                "OnNotifyIconBalloonClick",
                "OnNotifyIconContextItemSelected",
                "OnWindowChanged"
            ], Com.Wui.Framework.Commons.Interfaces.Events.IWebServiceClientEvents);
        }();
}

namespace Com.Wui.Framework.Services.Interfaces {
    "use strict";
    export let IWebServiceClient : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Send"
            ], Com.Wui.Framework.Commons.Interfaces.IWebServiceClient);
        }();
}

/* tslint:enable */
